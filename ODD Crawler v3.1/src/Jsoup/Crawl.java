package Jsoup;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Crawl {

	public static Document seed(String url) throws Exception {
		Document doc = Jsoup.connect(url).timeout(50000).get();
		return doc;
	}

	public static String getHeader(String str) throws IOException {
		Document docs = Jsoup.connect(str).get();
		String header = "";
		String title = docs.text();
		String h1 = docs.body().getElementsByTag("h1").text();
		String h2 = docs.body().getElementsByTag("h2").text();
		String h3 = docs.body().getElementsByTag("h3").text();
		String h4 = docs.body().getElementsByTag("h4").text();
		String h5 = docs.body().getElementsByTag("h5").text();
		String h6 = docs.body().getElementsByTag("h6").text();

		if (title.contains(h6) && !h6.equals("")) {
			header = "h6";
		} else if (title.contains(h2) && !h2.equals("")) {
			header = "h2";
		} else if (title.contains(h3) && !h3.equals("")) {
			header = "h3";
		} else if (title.contains(h4) && !h4.equals("")) {
			header = "h4";
		} else if (title.contains(h5) && !h5.equals("")) {
			header = "h5";
		} else if (title.contains(h1) && !h1.equals("")) {
			header = "h1";
		}
		return header;

	}

	public static String getStatus(String str) {
		int code = 0;
		HttpURLConnection connection;
		try {
			URL url = new URL(str);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();
			code = connection.getResponseCode();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return String.valueOf(code);
	}

}
