package Jsoup;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MetaData {

	public static String metaData(Document htmlDoc,String meta)
	{
		String metadata = "";
		try
		{
		metadata=htmlDoc.select("meta[name=" + meta + "]").first().attr("content");
		}
		catch(Exception e)
		{
			Elements paragraph = htmlDoc.select("p");
			for (Element p : paragraph)
				metadata = metadata.concat(p.text());
		}
		return metadata;
	}
}
