package removalofSpecial;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RemoveSpecialChar {
	public static String removeSpecialChars(String title)
    {
        String refinedTitle = null;
        Pattern p=Pattern.compile("[^0-9A-Za-z- ]+");
        Matcher m=p.matcher(title);
        if(m.find())
        {
            refinedTitle=m.replaceAll(" ");
            return refinedTitle;
        }
        else
            return title;
    }
}
