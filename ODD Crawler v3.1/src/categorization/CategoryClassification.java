package categorization;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;

import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayesMultinomialUpdateable;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class CategoryClassification implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String GetType(String text) {
		int n = 0;
		int index = 0;
		ArrayList<String> cat=new ArrayList<>();
		try {
			CategoryClassification cl = new CategoryClassification(new NaiveBayesMultinomialUpdateable());
			BufferedReader br = new BufferedReader(new FileReader("train.txt"));
			

			String line = null;
			while ((line = br.readLine()) != null) {

				String lines[] = line.split("!!");

				if (n == 0) {
					for (int i = 0; i < lines.length; i++) {

						cl.addCategory(lines[i]);
						cat.add(lines[i]);
					}
					n++;
					cl.setupAfterCategorysAdded();
				} else {
					cl.addData(lines[0], lines[1]);
				}

			}
			br.close();

			double[] result = cl.classifyMessage(text);
			double largest = result[0];
			
			for (int i = 1; i < result.length; i++) {
			  if ( result[i] > largest ) {
			      largest = result[i];
			      index = i;
			   }
			}
			//System.out.println("====== RESULT ====== \tCLASSIFIED AS:\t" + cat.get(index));
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		return cat.get(index);
	}

	private Instances trainingData;
	private StringToWordVector filter;
	private Classifier classifier;
	private boolean upToDate;
	private FastVector classValues;
	private FastVector attributes;
	private boolean setup;

	private Instances filteredData;

	public CategoryClassification(Classifier classifier) throws FileNotFoundException {
		this(classifier, 10);

	}

	public static void main(String[] args) {
		System.out.println(GetType("Parker Wallace OS X Objective-C lead developer (part-time) Sep 29"));
		System.out.println(GetType("Clevertech Driven JavaScript / Node.js Developer Sep 28"));
		System.out.println(GetType("need a massage theropist"));
		System.out.println(GetType("communication"));

	}

	public CategoryClassification(Classifier classifier, int startSize) throws FileNotFoundException {
		this.filter = new StringToWordVector();
		this.classifier = classifier;
		// Create vector of attributes.
		this.attributes = new FastVector(2);
		// Add attribute for holding texts.
		this.attributes.addElement(new Attribute("text", (FastVector) null));
		// Add class attribute.
		this.classValues = new FastVector(startSize);
		this.setup = false;

	}

	public void addCategory(String category) {
		category = category.toLowerCase();
		// if required, double the capacity.
		int capacity = classValues.capacity();
		if (classValues.size() > (capacity - 5)) {
			classValues.setCapacity(capacity * 2);
		}
		classValues.addElement(category);
	}

	public void addData(String message, String classValue) throws IllegalStateException {
		if (!setup) {
			throw new IllegalStateException("Must use setup first");
		}
		message = message.toLowerCase();
		classValue = classValue.toLowerCase();
		// Make message into instance.
		Instance instance = makeInstance(message, trainingData);
		// Set class value for instance.
		instance.setClassValue(classValue);
		// Add instance to training data.
		trainingData.add(instance);
		upToDate = false;
	}

	/**
	 * Check whether classifier and filter are up to date. Build i necessary.
	 * 
	 * @throws Exception
	 */
	private void buildIfNeeded() throws Exception {
		if (!upToDate) {
			// Initialize filter and tell it about the input format.
			filter.setInputFormat(trainingData);
			// Generate word counts from the training data.
			filteredData = Filter.useFilter(trainingData, filter);
			// Rebuild classifier.
			classifier.buildClassifier(filteredData);
			upToDate = true;
		}
	}

	public double[] classifyMessage(String message) throws Exception {
		message = message.toLowerCase();
		if (!setup) {
			throw new Exception("Must use setup first");
		}
		// Check whether classifier has been built.
		if (trainingData.numInstances() == 0) {
			throw new Exception("No classifier available.");
		}
		buildIfNeeded();
		Instances testset = trainingData.stringFreeStructure();
		Instance testInstance = makeInstance(message, testset);

		// Filter instance.
		filter.input(testInstance);
		Instance filteredInstance = filter.output();
		return classifier.distributionForInstance(filteredInstance);

	}

	private Instance makeInstance(String text, Instances data) {
		// Create instance of length two.
		Instance instance = new Instance(2);
		// Set value for message attribute
		Attribute messageAtt = data.attribute("text");
		instance.setValue(messageAtt, messageAtt.addStringValue(text));
		// Give instance access to attribute information from the dataset.
		instance.setDataset(data);
		return instance;
	}

	public void setupAfterCategorysAdded() {
		attributes.addElement(new Attribute("class", classValues));
		// Create dataset with initial capacity of 100, and set index of class.
		trainingData = new Instances("MessageClassificationProblem", attributes, 100);
		trainingData.setClassIndex(trainingData.numAttributes() - 1);
		setup = true;
	}

}