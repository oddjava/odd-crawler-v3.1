package code.crawlerPage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import translation.Translation;
import view.crawlerPage.CrawlerPage;

import Jsoup.Crawl;
import Jsoup.MetaData;

import apacheNLP.NLP;
import extractor.awsS3bucket.S3upload;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import crawlLogic.CrawlingSeeds;
import dateTime.Date;
import extractor.boiler.BoilerPipeMainText;
import extractor.imageExtractor.GetByGoose;
import extractor.ner.StanFordNER;
import mongoDBConnection.GetConnection;

public class CrawlerPageLogic implements ActionListener {
	DBCollection crawldataDB = GetConnection.connect("crawldata");

	DBCollection sampleCrawlDataDB = GetConnection.connect("sampleCrawldata");

	DBCollection seedUrlDB = GetConnection.connect("SeedUrl");

	DBCollection domainDB = GetConnection.connect("domain");

	DBCollection nlpFileNameDB = GetConnection.connect("nlpFilename");

	DBCollection settingsDb = GetConnection.connect("settings");

	// Map<String,String> modules = new HashMap<>();

	ArrayList<String> authorityLink = new ArrayList<>();
	ArrayList<String> seedUrl;
	ArrayList<String> crawlingSeeds;
	public static String[][] tableData;

	Task task;

	int progressCalc = 0;
	int prevProgress = 0;

	private class Task extends Thread {
		public Task() {

		}

		public void run() {

			for (int i = 0; i <= crawlingSeeds.size(); i++) {
				prevProgress = prevProgress + progressCalc;
				final int progress = prevProgress;
				final int j = i;
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						//CrawlerPage.progressBar.setValue(progress);
						int authorityCount = 0;

						// for (int i = 0; i < crawlingSeeds.size(); i++) {
						try {
							String domainName = "";
							String moduleName = "";
							String type = "";
							String keyword = "", description = "";
							String regex = "";
							Elements links = null;
							try {
								Document htmlDoc = null;
								try {
									BasicDBObject seedUrlQuery = new BasicDBObject();
									seedUrlQuery.put("url", crawlingSeeds.get(j));
									DBCursor seedUrlCursor = seedUrlDB.find(seedUrlQuery); // If
									// we
									// don
									// not
									// have
									// query
									// then
									// keep
									// brackets
									// blank.
									// It
									// will
									// fetch
									// all
									// records
									while (seedUrlCursor.hasNext()) {
										DBObject object = seedUrlCursor.next();
										domainName = object.get("domainUrl").toString();
										type = object.get("type").toString();
										moduleName = object.get("moduleName").toString();
									}

									BasicDBObject domainQuery = new BasicDBObject();
									domainQuery.put("domainName", domainName);
									DBCursor domainCursor = domainDB.find(domainQuery); // If
									// we
									// don
									// not
									// have
									// query
									// then
									// keep
									// brackets
									// blank.
									// It
									// will
									// fetch
									// all
									// records
									while (domainCursor.hasNext()) {
										// System.out.println(cursor.next().get("name"));
										DBObject object = domainCursor.next();
										keyword = object.get("keyword").toString();
										description = object.get("description").toString();
										regex = object.get("regex").toString();
									}

									if (!regex.contains("no regex")) {
										htmlDoc = Crawl.seed(crawlingSeeds.get(j));
										links = htmlDoc.select("a[href]");
										System.out.println("Connected to " + crawlingSeeds.get(j));
									}
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								for (Iterator iterator = links.iterator(); iterator.hasNext();) {
									Element link = (Element) iterator.next();
									String title = link.text();
									String authorityUrl = link.attr("abs:href");
									// authorityCount = authorityCount + 1;
									// System.out.println(title + " " +
									// authorityUrl);

									BasicDBObject sampleCrawlDataQuery = new BasicDBObject();
									sampleCrawlDataQuery.put("link", authorityUrl); // parameterized
									// query.
									// here
									// we
									// provide
									// the
									// condition
									DBCursor sampleCrawlDataCursor = sampleCrawlDataDB.find(sampleCrawlDataQuery); // If
									// we
									// don
									// not
									// have
									// query
									// then
									// keep
									// brackets
									// blank.
									// It
									// will
									// fetch
									// all
									// records
									if (!sampleCrawlDataCursor.hasNext()) { // Url
																			// not
										// present
										// in
										// Database
										if (authorityUrl.contains(regex)) {
											try {
												String prediction;
												String fileName = "";
												if (CrawlerPage.modules.get("nlp").contains("no")) {
													prediction = "required";
												} else {
													BasicDBObject nlpFileNameQuery = new BasicDBObject();
													nlpFileNameQuery.put("moduleName", moduleName);
													DBCursor nlpFileNameCursor = nlpFileNameDB.find(nlpFileNameQuery);
													// Print out "highest"
													// priority items
													while (nlpFileNameCursor.hasNext()) {
														DBObject object = nlpFileNameCursor.next();
														fileName = object.get("fileName").toString();
													}
													prediction = NLP.DocumentCategorizer(title, fileName);
												}
												if (prediction.contains("required")) {
													authorityLink.add(authorityUrl);

													String metaDescription = MetaData.metaData(htmlDoc, description);
													String metaKeyword = MetaData.metaData(htmlDoc, keyword);
													String translatedTitle, translatedDescription, translatedKeyword;
													if (CrawlerPage.modules.get("translation").contains("no")) {
														translatedTitle = "";
														translatedDescription = "";
														translatedKeyword = "";
													} else {
														if (!Translation.languageDetectIsEnglish(title)) {
															translatedTitle = Translation.languageTranslate(title);
														} else {
															translatedTitle = "";
														}
														if (!Translation.languageDetectIsEnglish(description)) {
															translatedDescription = Translation
																	.languageTranslate(description);
														} else {
															translatedDescription = "";
														}
														if (!Translation.languageDetectIsEnglish(keyword)) {
															translatedKeyword = Translation.languageTranslate(keyword);
														} else {
															translatedKeyword = "";
														}
													}
													String systemDate = Date.getDate(0);

													insertCrawlData(domainName, authorityUrl, title, metaDescription,
															metaKeyword, "", type, "", "", 0, "", "", translatedTitle,
															translatedDescription, translatedKeyword, "", "", "",
															systemDate, "", "", "", "", "", "", "", "", "", "", "", "",
															"", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
															"", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
															"", "", "", "", "", "", "", moduleName, "");
												}
											} catch (Exception e) {
												System.out.println("Due to some error, data was not in database");
												// e.printStackTrace();
											}
										}
									}

								}
								links = null;
							} catch (Exception e) {
								System.out.println("Cannot connect to " + crawlingSeeds.get(j));
							}
							// progressBar.setValue(percent);
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						// }

						if (CrawlerPage.modules.get("ner").contains("yes")) {
							BasicDBObject crawldataQuery = new BasicDBObject();
							for (int i = 0; i < authorityLink.size(); i++) {
								String location = "", personName = "", leadDate = "";
								String link = authorityLink.get(i);
								String mainContent = "";
								// Boilerpipe code will be here
								// NER code will be here and the values will be
								// stored in above mentioned variables.

								URL url = null;
								try {
									url = new URL(link);
								} catch (MalformedURLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								BoilerPipeMainText bpmt = new BoilerPipeMainText(url);
								mainContent = bpmt.process();
								System.out.println(url + " " + mainContent);
								StanFordNER stanford = new StanFordNER(mainContent);
								HashMap hm = stanford.getDataForAllClasses();
								leadDate = hm.get("date").toString();
								location = hm.get("location").toString();
								personName = hm.get("person").toString();

								if (personName.length() > 2)
									personName = personName.substring(1, personName.length() - 1);
								else
									personName = "-";

								if (location.length() > 2)
									location = location.substring(1, location.length() - 1);
								else
									location = "-";

								if (leadDate.length() > 2)
									leadDate = leadDate.substring(1, leadDate.length() - 1);
								else
									leadDate = "-";

								System.out.println(
										link + " " + location + " " + personName + " " + leadDate + " " + mainContent);
								crawldataQuery.put("link", link);

								DBCursor crawldataCursor = crawldataDB.find(crawldataQuery);
								if (crawldataCursor.hasNext()) {
									DBObject crawldataItem = crawldataCursor.next();
									crawldataItem.put("location", location);
									crawldataItem.put("personName", personName);
									crawldataItem.put("leadDate", leadDate);
									crawldataItem.put("mainContent", mainContent);
									crawldataDB.save(crawldataItem);
								}

								DBCursor sampleCrawldataCursor = sampleCrawlDataDB.find(crawldataQuery);
								if (sampleCrawldataCursor.hasNext()) {
									DBObject crawldataItem = sampleCrawldataCursor.next();
									crawldataItem.put("location", location);
									crawldataItem.put("personName", personName);
									crawldataItem.put("leadDate", leadDate);
									crawldataItem.put("mainContent", mainContent);
									sampleCrawlDataDB.save(crawldataItem);
								}

							}
						}
						if (CrawlerPage.modules.get("image").contains("yes")) {
							// Create a package to store extracted image on S3
							// bucket and store path of same in the database.
							// While storing we should store S3 bucket path only
							BasicDBObject crawldataQuery = new BasicDBObject();
							String imageSrc = "", fevicon = "";

							for (int i = 0; i < authorityLink.size(); i++) {
								String link = authorityLink.get(i);

								// Goose Code will be here
								// S3 bucket code should be here in order to
								// store extracted image on server.
								// Final S3 bucket path should be in imageSrc
								// var
								// Also code to fetch fevicon of a provided link
								// and store in fevicon var.
								GetByGoose getGoose = new GetByGoose(link);

								imageSrc = getGoose.ImageValue();

								if (imageSrc != "no.jpg") {
									try {
										new S3upload(imageSrc);
									} catch (IOException e) {

										e.printStackTrace();
									}

								}

								crawldataQuery.put("link", link);
								DBCursor crawldataCursor = crawldataDB.find(crawldataQuery);
								if (crawldataCursor.hasNext()) {
									DBObject crawldataItem = crawldataCursor.next();
									crawldataItem.put("image", imageSrc);
									crawldataItem.put("fevicon", fevicon);
									crawldataItem.put("localImagepath", imageSrc);
									crawldataDB.save(crawldataItem);
								}

								DBCursor sampleCrawldataCursor = sampleCrawlDataDB.find(crawldataQuery);
								if (sampleCrawldataCursor.hasNext()) {
									DBObject crawldataItem = sampleCrawldataCursor.next();
									crawldataItem.put("image", imageSrc);
									crawldataItem.put("fevicon", fevicon);
									sampleCrawlDataDB.save(crawldataItem);
								}

							}
						}

						/*
						 * TableModel model = new DefaultTableModel(tableData,
						 * header) { public boolean isCellEditable(int row, int
						 * column) { return false;// This causes all cells to be
						 * not editable } };
						 */
						// new CrawlerPage().tableStats(model);
					}
				});
				try {
					Thread.sleep(100);
					System.out.println("Sleeping");
				} catch (InterruptedException e) {
				}
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		authorityLink.clear();

		//seedUrl = CrawlingSeeds.dbSeedUrl();

		//crawlingSeeds = CrawlingSeeds.crawlSeedUrl(seedUrl);
		progressCalc = 100 / crawlingSeeds.size();
		if (event.getActionCommand().equals("Start")) {
			task = new Task();
			task.start();
		}
	}

	public void insertCrawlData(String domainName, String authorityUrl, String title, String description,
			String keyword, String leadDate, String mainCategory, String subCategory, String type, int is_star,
			String expiredDate, String expiredStatus, String translatedTitle, String translatedDescription,
			String translatedKeywords, String location, String regions, String personName, String systemDate,
			String image, String fevicon, String companyName, String specialist, String industry, String companySize,
			String founded, String alexaRanking, String webServer, String hostingProvider, String framework,
			String SSlCertificate, String language, String nameServer, String investorName, String status,
			String investments, String fundRaised, String alaises, String comapanyEmail, String companyNumber,
			String rating, String starring, String version, String size, String compatibility, String amount,
			String pages, String ISBN, String OCLC, String preceddedBy, String followedBy, String followersCount,
			String twitter_link, String linkedin_link, String facebook_link, String gmail_link, String twitter_image,
			String linkedin_image, String facebook_image, String gmail_image, String twitter_location,
			String linkedin_location, String facebook_location, String gmail_location, String developers,
			String ownership, String LCClass, String mediatype, String moduleName, String mainContent) {
		BasicDBObject insertObject = new BasicDBObject();
		insertObject.put("domainLink", domainName);
		insertObject.put("link", authorityUrl);
		insertObject.put("title", title);
		insertObject.put("description", description);
		insertObject.put("keyword", keyword);
		insertObject.put("leadDate", "");
		insertObject.put("mainCategory", type);
		insertObject.put("subCategory", "");
		insertObject.put("type", "");
		insertObject.put("is_star", 0);
		insertObject.put("expiredDate", "");
		insertObject.put("expiredStatus", "");
		insertObject.put("translatedTitle", "");
		insertObject.put("translatedDescription", "");
		insertObject.put("translatedKeyword", "");
		insertObject.put("location", "");
		insertObject.put("regions", "");
		insertObject.put("personName", "");
		insertObject.put("systemDate", systemDate);
		insertObject.put("image", "");
		insertObject.put("fevicon", "");
		insertObject.put("companyName", "");
		insertObject.put("specialist", "");
		insertObject.put("industry", "");
		insertObject.put("companySize", "");
		insertObject.put("founded", "");
		insertObject.put("alexaRanking", "");
		insertObject.put("webServer", "");
		insertObject.put("hostingProvider", "");
		insertObject.put("framework", "");
		insertObject.put("SSLCertificate", "");
		insertObject.put("language", "");
		insertObject.put("nameServer", "");
		insertObject.put("investorName", "");
		insertObject.put("status", "");
		insertObject.put("investments", "");
		insertObject.put("fundRaised", "");
		insertObject.put("alaises", "");
		insertObject.put("companyEmail", "");
		insertObject.put("companyNumber", "");
		insertObject.put("rating", "");
		insertObject.put("starring", "");
		insertObject.put("version", "");
		insertObject.put("size", "");
		insertObject.put("compatibility", "");
		insertObject.put("amount", "");
		insertObject.put("pages", "");
		insertObject.put("ISBN", "");
		insertObject.put("OCLC", "");
		insertObject.put("precededBy", "");
		insertObject.put("followedBy", "");
		insertObject.put("followersCount", "");
		insertObject.put("twitter_link", "");
		insertObject.put("linkedin_link", "");
		insertObject.put("facebook_link", "");
		insertObject.put("gmail_link", "");
		insertObject.put("twitter_image", "");
		insertObject.put("linkedin_image", "");
		insertObject.put("facebook_image", "");
		insertObject.put("gmail_image", "");
		insertObject.put("twitter_location", "");
		insertObject.put("linkedin_location", "");
		insertObject.put("facebook_location", "");
		insertObject.put("gmail_location", "");
		insertObject.put("developers", "");
		insertObject.put("ownership", "");
		insertObject.put("LCClass", "");
		insertObject.put("mediaType", "");
		insertObject.put("moduleName", moduleName);
		insertObject.put("mainContent", "");
		insertObject.put("localImagepath", "");
		sampleCrawlDataDB.insert(insertObject);
		crawldataDB.insert(insertObject);
		insertObject.clear(); // It
								// needs
								// to
								// get
								// clear
								// to
								// update
								// next
								// records;
	}
}