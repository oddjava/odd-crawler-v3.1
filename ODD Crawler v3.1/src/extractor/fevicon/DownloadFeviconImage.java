package extractor.fevicon;

import static com.mongodb.client.model.Filters.eq;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;

import javax.print.Doc;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.mongodb.client.MongoDatabase;

import ch.qos.logback.core.net.SyslogOutputStream;

import com.mongodb.Block;
import com.mongodb.client.FindIterable;

public class DownloadFeviconImage {

	static String filePath = ".\\Images\\";
	static String fileNameInProperFormat = "www_domainName_com.extention";
	static String imageName = "Not Found";
	static String url = "Not Found";
	static String fullFileName = "fileName.extention";

	public static String getDomainName() {
		return fileNameInProperFormat;
	}

	public static void setDomainName(String domainName) {
		DownloadFeviconImage.fileNameInProperFormat = domainName;
	}

	public static String getFilePath() {
		return filePath;
	}

	public static void setFilePath(String filePath) {
		DownloadFeviconImage.filePath = filePath;
	}

	public static String getFavImage() {
		return imageName;
	}

	public static void setFavImage(String favImage) {
		DownloadFeviconImage.imageName = favImage;
	}

	public static String downloadFaviconImageAndGetPath(Document document, String domainName) throws IOException {
		//url = link;
		// TODO Auto-generated method stub
		// Fetching domain name from Database
		fileNameInProperFormat = domainName;
		/*
		 * if(url.matches("https")) { url="https://"+fileNameInProperFormat+"/";
		 * System.out.println(url); } else {
		 * url="http://"+fileNameInProperFormat+"/"; System.out.println(url); }
		 */

		if (!fileNameInProperFormat.equals(null)) {
			// Changing Domain name in file format name

			fileNameInProperFormat = fileNameInProperFormat.replace('.', '_');
			// System.out.println(imageName);

		}

		if (DownloadFeviconImage.searchFile(fileNameInProperFormat)) {
			System.out.println("file found in folder");
			return filePath + "\\" + fullFileName;
		} else {
			Element element = document.head().select("link[href~=.*\\.(ico|png)]").first(); // check
																							// this
																							// link
																							// for
																							// more
																							// information
																							// about
																							// how
																							// to
																							// find
																							// favicon
																							// image
																							// and
																							// what
																							// are
																							// there
																							// tag
																							// ,attribute
																							// and
																							// Image
																							// format
			if (element == null) // https://recalll.co/app/?q=How%20to%20get%20the%20website%20image/logo%20with%20jsoup%20in%20Android
			{
				element = document.head().select("meta[itemprop=image]").first();
				if (element != null) {
					imageName = element.attr("abs:content");
				}
			} else {
				imageName = element.attr("abs:href");

			}

			System.out.println(imageName);

			if (!imageName.equals("Not Found")) {

				try {
					return DownloadFeviconImage.downloadImageAndSaveAndReturnPath(imageName);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("IoException Found In DownloadFaviconImageAndGetPath() method 2");
					// e.printStackTrace();
				}

			}

			return imageName;
		}
	}

	private static String downloadImageAndSaveAndReturnPath(String src) throws IOException {

		String fileName = null;

		// Exctract the name of the image from the src attribute
		int indexname = src.lastIndexOf("/");

		if (indexname == src.length()) {
			src = src.substring(1, indexname);
		}

		indexname = src.lastIndexOf("/");

		fileName = src.substring(indexname + 1, src.length());
		System.out.println("File Name - " + fileName);

		// Open a URL Stream

		URL url = new URL(src);

		InputStream in = url.openStream();
		OutputStream out = new BufferedOutputStream(new FileOutputStream(filePath + fileName));

		for (int b; (b = in.read()) != -1;) { // -1 means end of file

			out.write(b);

		}

		out.close();
		in.close();

		// changing file name according to domain

		fileNameInProperFormat = fileNameInProperFormat
				+ fileName.substring((fileName.lastIndexOf('.')), fileName.length());

		File oldFileName = new File(filePath + fileName);
		File newFileName = new File(filePath + fileNameInProperFormat);

		if (oldFileName.renameTo(newFileName)) {
			System.out.println("Rename file Successfully");
		} else {
			System.out.println("Rename file failed");
		}

		return filePath + fileNameInProperFormat;

	}

	private static Boolean searchFile(String fileName) throws IOException {

		int lastdot = 0;
		String avalibleFileName = null;

		File folder = new File(DownloadFeviconImage.getFilePath());

		if (folder.isDirectory()) {
			File[] listOfFiles = folder.listFiles();
			for (File file : listOfFiles) {
				avalibleFileName = file.getName();
				lastdot = avalibleFileName.lastIndexOf(".");
				avalibleFileName = avalibleFileName.substring(0, lastdot);
				if (avalibleFileName.equals(fileName)) {
					fullFileName = file.getName();
					return true;
				}

			}
		} else {
			return false;
		}
		return false;
	}

	/*public static void main(String[] args) {

		String folderPath;
		try {
			folderPath = DownloadFeviconImage.downloadFaviconImageAndGetPath(
					"http://www.postjobfree.com/job/a5c91k/ux-designer-freelance-ui-los-angeles-ca");
			System.out.println(folderPath);
		} catch (Exception e) {

			System.out.println("don't put wrong url format or check your database connetion");
		}

		
		 * folderPath= DownloadFeviconImage.downloadFaviconImageAndGetPath(
		 * "http://www.quikr.com/"); System.out.println(folderPath);
		 * DownloadFeviconImage.downloadFaviconImageAndGetPath(
		 * "https://www.olx.in/"); System.out.println(folderPath);
		 * DownloadFeviconImage.downloadFaviconImageAndGetPath(
		 * "https://www.google.co.in/"); System.out.println(folderPath);
		 
		// System.out.println(DomainName.getMainDomainName("http://ahmedabad.quikr.com/freelancer-jobs-freelancers/freelancer-jobs/x18233770"));
		// System.out.println(DomainName.getSubDomainName("http://ahmedabad.quikr.com/freelancer-jobs-freelancers/freelancer-jobs/x18233770"));

	}*/

}