package extractor.imageExtractor;

import com.gravity.goose.Article;
import com.gravity.goose.Configuration;
import com.gravity.goose.Goose;
import com.gravity.goose.images.Image;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;

public class GooseImageExtraction {


	private static final String folderPath = "./Images";
	Goose goose = new Goose(new Configuration());
	Article article;
	static String url;
	String name1;


	public  void ImageExtraction(String link)
	{
		url = link;
		//url="http://www.cnet.com/news/will-these-smart-small-appliances-make-cooking-easier/";
		//GooseImageExtraction gooseimageextraction = new GooseImageExtraction();

		try {
			FileInputStream imageInputStream = this.getImage(url);
			//String mainText = this.getText(url);
		} 
		catch 
		(IOException e) 
		{
			//e.printStackTrace();
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}

	}



	public FileInputStream getImage(String url) throws IOException {
		this.article = this.goose.extractContent(url);
		String src = this.article.topImage().imageSrc();
		//System.out.println(src);
		FileInputStream fileinputstreamImg = this.saveImage(src);
		return fileinputstreamImg;
	}

	public FileInputStream saveImage(String src) throws IOException {
		FileInputStream fis;
		InputStream inStream = null;
		OutputStream outStream = null;
		fis = null;
		int indexname = src.lastIndexOf("/");
		if (indexname == src.length()) {
			src = src.substring(1, indexname);
		}
		indexname = src.lastIndexOf("/");
		String name = src.substring(indexname, src.length());
		System.out.println("Name of a file.." + name);
		try {
			try {
				int b;
				URL url1 = new URL(src);
				inStream = url1.openStream();
				outStream = new BufferedOutputStream(new FileOutputStream("./Images/" + name));

				while ((b = inStream.read()) != -1) {
					outStream.write(b);
				}
				System.out.println("File is been saved");
				name1 = name.substring(1);
				File file = new File("./Images/" + name1);
				System.out.println(url);
				
				System.out.println("File Name1 "+name1);
				fis = new FileInputStream(file);

				System.out.println(fis.toString());
			} catch (Exception e) {
				//e.printStackTrace();
				System.out.println(e.getMessage());
				outStream.close();
				inStream.close();
			}
		} finally {
			outStream.close();
			inStream.close();
		}
		return fis;
	}

	public String getText(String url) {
		try
		{
			this.article = this.goose.extractContent(url);
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("ok");
		}

		String articleMainText = this.article.cleanedArticleText();
		System.out.println("\n\ntext:>>" + this.article.cleanedArticleText());
		return articleMainText;
	}
	public String getImageName(){
	 
		return name1;
	}
}
