package extractor.imageExtractor;

import com.gravity.goose.Article;
import com.gravity.goose.Configuration;
import com.gravity.goose.Goose;

public class GetByGoose {

	static String link;

	public GetByGoose(String link) {
		this.link = link;
	}

 public	 String ImageValue() {
		Goose goose = new Goose(new Configuration());
		Article article;
		article = goose.extractContent(link);
		String src = article.topImage().imageSrc();

		if (src == "") {

			return "no.jpg";
		} else {
			GooseImageExtraction newgoose = new GooseImageExtraction();
			newgoose.ImageExtraction(link);

			return newgoose.getImageName();
		}
	}
 
 public static void main(String[] args) {
	System.out.println("Check this out "+new GetByGoose("http://www.cnet.com/news/netflix-admits-throttling-video-speeds-on-at-t-verizon/").ImageValue());
}
}