package extractor.boiler;


import java.net.MalformedURLException;
import java.net.URL;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import de.l3s.boilerpipe.extractors.DefaultExtractor;
import de.l3s.boilerpipe.extractors.KeepEverythingExtractor;
import de.l3s.boilerpipe.extractors.LargestContentExtractor;

public class BoilerPipeMainText
{
	URL urlToProcess;
	String textToProcess;
	public BoilerPipeMainText(URL textToProcess)
	{
		this.urlToProcess=textToProcess;
		
	}
	@SuppressWarnings("finally")
	public String process()
	{
		try 
		{
			//System.out.println("ALo re ");
			textToProcess=ArticleExtractor.INSTANCE.getText(urlToProcess);
			 
			/*textToProcess=DefaultExtractor.INSTANCE.getText(urlToProcess);
			System.out.println("Text to process "+textToProcess);
			
			textToProcess=LargestContentExtractor.INSTANCE.getText(urlToProcess);
			System.out.println("Text to process "+textToProcess);
			
			textToProcess=KeepEverythingExtractor.INSTANCE.getText(urlToProcess);
			System.out.println("Text to process "+textToProcess);*/
			 
		} catch (BoilerpipeProcessingException e) 
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("No Main Content");
			e.printStackTrace();
			textToProcess="";
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("EXCEPTION");
		}
		finally
		{
			return textToProcess;
		}
		
	}
	
	public static void main(String[] args) throws MalformedURLException {
		new BoilerPipeMainText(new URL("http://www.postjobfree.com/job/a5erje/freelance-architect-ux-user-englewood-co")).process();
	}
	
}
