package addDomainLogic;

public class SeedUrl {

	private String url;
	private String ModuleName;
	private String SystemName;
	private String addedDate;
	private String subDomainOrMaindomain;
	private int bucketNumber;
	private String nextCrawlDate;
	private String keyWords;
	private String description;
	private String regx;
	private String type;
	
	public SeedUrl() {
		// TODO Auto-generated constructor stub
		this.regx = "no regex";
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getModuleName() {
		return ModuleName;
	}
	public void setModuleName(String moduleName) {
		ModuleName = moduleName;
	}
	public String getSystemName() {
		return SystemName;
	}
	public void setSystemName(String systemName) {
		SystemName = systemName;
	}
	public String getAddedDate() {
		return addedDate;
	}
	public void setAddedDate(String addedDate) {
		this.addedDate = addedDate;
	}
	public String getSubDomainOrMaindomain() {
		return subDomainOrMaindomain;
	}
	public void setSubDomainOrMaindomain(String subDomainOrMaindomain) {
		this.subDomainOrMaindomain = subDomainOrMaindomain;
	}
	public int getBucketNumber() {
		return bucketNumber;
	}
	public void setBucketNumber(int bucketNumber) {
		this.bucketNumber = bucketNumber;
	}
	public String getNextCrawlDate() {
		return nextCrawlDate;
	}
	public void setNextCrawlDate(String nextCrawlDate) {
		this.nextCrawlDate = nextCrawlDate;
	}
	public String getKeyWords() {
		return keyWords;
	}
	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRegx() {
		return regx;
	}
	public void setRegx(String regx) {
		this.regx = regx;
	}
	
	public String toString() {
		System.out.println("");
        return String.format("%s - %s - %s - %s - %s - %s - %s ",url,keyWords,description,ModuleName,subDomainOrMaindomain,addedDate,regx );
    }
	
	
	
}
