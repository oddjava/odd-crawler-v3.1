package addDomainLogic;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.monitorjbl.xlsx.StreamingReader;

public class SimpleExcelReaderExample {
	
	
	public List<SeedUrl> readBooksFromExcelFile(String excelFilePath) throws IOException, OpenXML4JException {
	    int rowCount=0;
		List<SeedUrl> seedUrlData = new ArrayList<SeedUrl>();
	    FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
	 
	   // Workbook workbook = new XSSFWorkbook(inputStream); //it will accept only still 1mb file if you want to give bigger than 1mb excel file then use  XSSFReader 
	   // Sheet firstSheet = workbook.getSheetAt(0);
	   // Iterator<Row> iterator = firstSheet.iterator();
	   
	   /* OPCPackage pkg = OPCPackage.open(inputStream);
	    XSSFReader bigreader=new XSSFReader(pkg);
	    XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator) bigreader.getSheetsData();
	    while (iter.hasNext()) {
            InputStream stream = iter.next();
            String sheetName = iter.getSheetName();
            System.out.println(sheetName);
            
	    }
	    */
	    
	   //  int Rowcount=sheet.getPhysicalNumberOfRows();
	    //System.out.println(Rowcount);
	  
	    
	    StreamingReader reader = StreamingReader.builder()//using SteamingReader class we can read big excel file
		        .rowCacheSize(200)    // number of rows to keep in memory (defaults to 10)
		        .bufferSize(8192)     // buffer size to use when reading InputStream to file (defaults to 1024)
		        .sheetIndex(0)        // index of sheet to use (defaults to 0)
		        .read(inputStream);            // InputStream or File for XLSX file (required)

	    Iterator<Row> iterator = reader.iterator();
	    
	  
	    while (iterator.hasNext()) { // skip first row because of we don't need heading
	    	if(rowCount==0)
	    	{
	    		iterator.next();
	    		rowCount=1;
	    	}
	        Row nextRow = iterator.next();
	        Iterator<Cell> cellIterator = nextRow.cellIterator(); 
	        SeedUrl seedUrl = new SeedUrl();
	 
	        while (cellIterator.hasNext()) {
	            Cell nextCell = cellIterator.next();   
	            int columnIndex = nextCell.getColumnIndex();// it will give column number and if you put data in Excel file from 1st column then it will give 0 and so on
	         //   System.out.println("Column Index "+columnIndex+"value"+nextCell);
	           /* if(nextRow.getCell(columnIndex)==null)
	            {
	               
	            }*/
	 
	            switch (columnIndex) {
	            case 0: 
	                seedUrl.setUrl((String)getCellValue(nextCell));                
	                break;
	            case 1:
	            	 seedUrl.setKeyWords((String)getCellValue(nextCell));
	                break;
	            case 2:
	                seedUrl.setDescription((String)getCellValue(nextCell));
	                break;
	            case 3:
	            	seedUrl.setModuleName((String)getCellValue(nextCell));
	            	break;
	            case 4:
	            	seedUrl.setSubDomainOrMaindomain((String)getCellValue(nextCell));
	            	break;
	            case 5:
	            	seedUrl.setSystemName((String)getCellValue(nextCell));
	            	break;
	            case 6:
					seedUrl.setType((String) getCellValue(nextCell));
					break;
	            }
	 
	 
	        }
	        seedUrlData.add(seedUrl);
	    }
	 
	    reader.close();
	    inputStream.close();
	    System.out.println(rowCount);
	    return seedUrlData;
	}
	
	
	private Object getCellValue(Cell cell) {
	    switch (cell.getCellType()) {
	    case Cell.CELL_TYPE_STRING:
	        return cell.getStringCellValue();
	 
	    case Cell.CELL_TYPE_BOOLEAN:
	        return cell.getBooleanCellValue();
	 
	    case Cell.CELL_TYPE_NUMERIC:
	        return cell.getNumericCellValue();
	    case Cell.CELL_TYPE_ERROR:
	    	return  cell.getRichStringCellValue();
	    }
	 
	    return null;
	}
	


}
