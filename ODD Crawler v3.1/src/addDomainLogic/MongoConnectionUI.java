package addDomainLogic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.bson.Document;

import com.mongodb.DBCollection;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import mongoDBConnection.GetConnection;

public class MongoConnectionUI extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JButton UploadData=null;

	public MongoConnectionUI()
	{
	}



	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		System.out.println("In Action Performed");
		String systemDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString(); 
		if(event.getActionCommand().equals("Add new Domain"))
		{
			File selectedFile=null;
			SimpleExcelReaderExample reader = null;
			List<SeedUrl> seedUrlData = null;
			JOptionPane.showMessageDialog(this,"Please Add Excel File which is in proper format.\nDo not add in url column Dublicate records in Excel File,\nIf there is any dublicate records,\nthen please remove it using Excel tool \"remove Duplicates\"");
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel Files","xlsx");
			fileChooser.setFileFilter(filter);
			int result = fileChooser.showOpenDialog(this);
			if (result == JFileChooser.APPROVE_OPTION) {
				selectedFile = fileChooser.getSelectedFile();
				System.out.println("Selected file: " + selectedFile.getAbsolutePath());
				reader= new SimpleExcelReaderExample();
				try {
					seedUrlData= reader.readBooksFromExcelFile(selectedFile.getAbsolutePath());
					MongoDatabase db=ConnectMongoDB.connectToMongoDB();
					/*DBCollection seedUrl = GetConnection.connect("SeedUrl");
					DBCollection domain = GetConnection.connect("domain");*/
					//db.getCollection("SeedUrl").createIndex("url",new Doc);
					Iterator<SeedUrl> data=seedUrlData.iterator();
					while(data.hasNext())
					{
						SeedUrl temp=data.next();

						System.out.println("Domain Type -  "+temp.getSubDomainOrMaindomain()+"  Url -  "+temp.getUrl());

						try
						{
							db.getCollection("SeedUrl").insertOne(new Document("url",temp.getUrl()).
									append("domainUrl", DomainName.getDomainName(temp.getSubDomainOrMaindomain(), temp.getUrl())).
									append("moduleName", temp.getModuleName()).
									append("systemName", temp.getSystemName()).
									append("addedDate", systemDate).
									append("subdomain/maindomain",temp.getSubDomainOrMaindomain()).
									append("bucketNumber", 0).
									append("nextCrawlDate", systemDate));

							db.getCollection("domain").insertOne(new Document("domainName", DomainName.getDomainName(temp.getSubDomainOrMaindomain(),temp.getUrl()))
									.append("keyword",temp.getKeyWords())
									.append("description",temp.getDescription())
									.append("regex","no regex").append("systemName",temp.getSystemName())
									.append("addedDate",systemDate));
						}
						catch(MongoWriteException e)
						{
							//data.next();
							System.out.println("Find duplicate value");
							System.out.println("Data size is "+seedUrlData.size());
							//e.printStackTrace();
						}
						catch(NullPointerException e)
						{
							System.out.println("Null pointer");
							System.out.println("Data size is "+seedUrlData.size());
						}

					}
					




				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (OpenXML4JException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}
			else
			{
				JOptionPane.showMessageDialog(this, "You did not selecte file please select Excel file for upload data");

			}












			//db.getCollection("").



			System.out.println("Upload Data Succesfullly");


		}


	}

}
