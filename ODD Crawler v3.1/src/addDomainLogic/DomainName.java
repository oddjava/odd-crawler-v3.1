package addDomainLogic;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DomainName {

	public static String getMainDomainName(String url) {
		try {
			URL url1 = new URL(url);
			String host = url1.getHost();
			// String netHost=host.substring(host.indexOf(".")+1);
			return host;
		} catch (Exception e) {
			return "Invalid Url in DomainName";
		}
	}

	//Ignore these methods. Not used anywhere
	public static String getSubDomainName(String url) throws URISyntaxException {
		String w="www.";
		Pattern p = Pattern.compile(".*?([^.]+\\.[^.]+)");
		URI uri = new URI(url);
		// eg: uri.getHost() will return "www.foo.com"
		Matcher m = p.matcher(uri.getHost());
		if (m.matches()) {
		}
		return w+m.group(1);
	}
	
	 static String getDomainName(String urlType,String url) throws URISyntaxException
		{
				
				
					if(urlType.equals("main"))
					{
					   return  getMainDomainName(url);
					}
					else
					{
						return  getSubDomainName(url);
					}	
		}

	public static void main(String[] args) throws URISyntaxException {
		
		
		System.out.println(getSubDomainName("http://www.devnet.Kentico.com"));
		System.out.println(getSubDomainName("http://www.bremen.craigslist.org"));
		System.out.println(getSubDomainName("http://www.tanger.locanto.ma"));
		System.out.println(getSubDomainName("http://western.locanto.com"));
		
		System.out.println("");
		
		System.out.println(getMainDomainName("https://www.dice.com/jobs?q=telecommute&l="));
		System.out.println(getMainDomainName("http://www.simplyhired.com"));
		System.out.println(getMainDomainName("http://www.classifieds.co.zw"));
		System.out.println(getMainDomainName("http://www.olx.in"));
		

		
	}
}



