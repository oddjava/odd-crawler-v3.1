package addDomainLogic;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class ConnectMongoDB {

	public static MongoDatabase connectToMongoDB() 
	{

		try{

			// To connect to mongodb server
			MongoClient mongoClient = new MongoClient("localhost" , 27017 );
			//MongoDatabase db=mongoClient.getDatabase("crawler");
			MongoDatabase db=mongoClient.getDatabase("crawler");
			System.out.println("Connect to database successfully");
			return db;
			
			//db.getCollection("mongo").insertOne(new Document("id",4).append("name","monu").append("age",25));
			// Now connect to your databases
			// DB db = mongoClient.getDB( "ganesh" );//for old driver befor 3 version
			//  boolean auth = db.authenticate("s", "s");
			// System.out.println("Authentication: "+auth);
			//  DBCollection coll = db.createCollection("mycol");
			//System.out.println("Collection created successfully");

		}catch(Exception e){
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.out.println("Connection Failed Please check your MonogDB setting");
		}
		return null;

	}
	
	public static MongoDatabase connectToMongoDB(String hostName,int portNumber,String databaseName) 
	{

		try{

			// To connect to mongodb server
			MongoClient mongoClient = new MongoClient(hostName,portNumber);
			MongoDatabase db=mongoClient.getDatabase(databaseName);
			
			System.out.println("Connect to database successfully");
			
			return db;
			
			//db.getCollection("mongo").insertOne(new Document("id",4).append("name","monu").append("age",25));
			// Now connect to your databases
			// DB db = mongoClient.getDB( "ganesh" );//for old driver befor 3 version
			//  boolean auth = db.authenticate("s", "s");
			// System.out.println("Authentication: "+auth);
			//  DBCollection coll = db.createCollection("mycol");
			//System.out.println("Collection created successfully");

		}catch(Exception e){
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.out.println("Connection Failed Please check your MonogDB setting");
		}
		return null;

	}


}
