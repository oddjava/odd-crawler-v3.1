package menu;

import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import com.mongodb.BasicDBObject;

import view.SystemStats.systemStatsPage;
import view.crawlerPage.CrawlerPage;
import view.domainStats.DomainStatsPage;
import view.frameSetting.JFrameSettings;
import view.login.LoginPage;
import view.trainingSetPage.TrainingSetPage;

public class MenuBar {
	static JMenuBar menu;

	static JMenu dashboard;
	static JMenu stats;
	static JMenu training;
	static JMenu verification;
	static JMenu settings;
	static JMenu logout;

	static JMenuItem systemStats;
	static JMenuItem domainStats;
	
	public static JMenuBar getPanel() {
		menu = new JMenuBar();
		dashboard = new JMenu("Dashboard");
		stats = new JMenu("Stats");
		training = new JMenu("Training Sets");
		verification = new JMenu("Verification");
		settings = new JMenu("Settings");
		logout = new JMenu("Logout");
		systemStats = new JMenuItem("System Stats");
		domainStats = new JMenuItem("Domain Stats");

		stats.add(systemStats);
		stats.add(domainStats);
		menu.add(dashboard);
		menu.add(stats);
		menu.add(training);
		menu.add(verification);
		menu.add(settings);
		menu.add(logout);
		return menu;
	}
}
