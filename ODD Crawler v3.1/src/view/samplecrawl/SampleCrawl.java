package view.samplecrawl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.bson.BSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import mongoDBConnection.GetConnection;
import view.domainStats.DomainStatsPage;
import view.frameSetting.JFrameSettings;
import view.topPanel.Navigate;
import view.topPanel.TopPanel;

/**
 * This page is for adding regex to selected domain
 * 
 * @author sourabh
 *
 */
public class SampleCrawl extends JFrame implements ActionListener {
	private JFrame sampleCrawlFrame = JFrameSettings.settings("Sample Crawl");
	private Task task;
	DBCollection coll = GetConnection.connect("SeedUrl");
	DBCollection coll1 = GetConnection.connect("domain");
	JMenuBar newMenuBar;
	JLabel label1 = new JLabel("Domain Name:");
	JLabel label2 = new JLabel("Authority Pages:");
	JTextArea pages = new JTextArea();
	JLabel label3 = new JLabel("Seed URL:");
	JLabel label4 = new JLabel("Pattern:");
	JButton button1 = new JButton("Generate Regex");
	JButton apply = new JButton("Apply");
	JButton back = new JButton("Back");
	JTextField regex = new JTextField();
	int n = 0;
	JButton button2 = new JButton("Initial Crawl");
	JProgressBar progressBar = new JProgressBar(0, 100);
	String[][] data;
	String domain, column[] = { "Title", "Link" }, link;
	JLabel domainlabel;
	JScrollPane sp;
	Document doc;
	Elements links;
	JTable jt;
	public ArrayList<node> newnode = new ArrayList<>();
	JComboBox<Object> combo;

	/**
	 * 
	 * @param domain
	 *            Domain to add regex
	 */
	public SampleCrawl(String domain) {
		this.domain = domain;
		SampleCrawlGUI();

	}

	public static void main(String[] args) {
		new SampleCrawl("www.global-free-classified-ads.com");
	}

	/**
	 * For displaying frame and its component
	 */
	void SampleCrawlGUI() {
		DBCursor cur=coll1.find(new BasicDBObject("domainName",domain));
		String reg=cur.next().get("regex").toString();
		regex.setText(reg);

		newMenuBar = TopPanel.getPanel();
		sampleCrawlFrame.setJMenuBar(newMenuBar);

		label1.setBounds(100, 50, 100, 20);
		sampleCrawlFrame.add(label1);
		domainlabel = new JLabel(domain);
		domainlabel.setBounds(300, 50, 200, 20);
		sampleCrawlFrame.add(domainlabel);
		label2.setBounds(700, 50, 100, 20);
		sampleCrawlFrame.add(label2);

		pages.setBounds(900, 50, 400, 80);
		sampleCrawlFrame.add(pages);

		button1.setBounds(1100, 150, 200, 20);
		sampleCrawlFrame.add(button1);
		button1.addActionListener(this);

		label3.setBounds(100, 200, 100, 20);
		sampleCrawlFrame.add(label3);

		ArrayList<String> set = new ArrayList<>();
		DBCursor cursor = coll.find();
		while (cursor.hasNext()) {
			DBObject present = cursor.next();
			if (present.get("url").toString().contains(domain)) {
				set.add(present.get("url").toString());// Getting all urls of
														// domain name in
														// arraylist

			}

		}
		Object[] set1 = set.toArray();
		combo = new JComboBox<>(set1);
		combo.setBounds(200, 200, 450, 20);
		sampleCrawlFrame.add(combo);

		label4.setBounds(700, 200, 100, 20);
		sampleCrawlFrame.add(label4);

		regex.setBounds(900, 200, 400, 20);
		
		
		sampleCrawlFrame.add(regex);

		button2.setBounds(400, 250, 100, 20);
		sampleCrawlFrame.add(button2);
		button2.addActionListener(this);

		back.setBounds(100, 600, 100, 20);
		sampleCrawlFrame.add(back);
		back.addActionListener(this);

		apply.setBounds(1200, 600, 100, 20);
		sampleCrawlFrame.add(apply);
		apply.addActionListener(this);

		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setBounds(600, 250, 500, 20);
		sampleCrawlFrame.add(progressBar);
		sampleCrawlFrame.repaint();

		Navigate navigate = new Navigate(sampleCrawlFrame);
		TopPanel.dashboard.addMenuListener(navigate);
		TopPanel.settings.addMenuListener(navigate);
		TopPanel.logout.addMenuListener(navigate);
		TopPanel.training.addMenuListener(navigate);
		TopPanel.verification.addMenuListener(navigate);
		TopPanel.domainStats.addActionListener(navigate);
		TopPanel.systemStats.addActionListener(navigate);

	}

	@Override
	/**
	 * This is action listener for all buttons
	 */
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == button2) {
			link = combo.getSelectedItem().toString();// getting selected link
														// from combobox
			newnode.clear();// clearing data from arraylist
			if (n > 0) {
				sampleCrawlFrame.remove(sp);// removing scroll pane from frame
				sampleCrawlFrame.repaint();
			}
			n++;
			try {
				doc = Jsoup.connect(link).timeout(500000).get();// jsoup
			} catch (IOException e1) {

				e1.printStackTrace();
			}
			links = doc.select("a[href]");

			for (Element lik : links) {
				if (lik.attr("abs:href").contains(regex.getText())) {// comparing
																		// all
																		// links
																		// to
																		// regex
					node node1 = new node();// creating node for individual link
					node1.text = lik.text();// storing text in node
					node1.url = lik.attr("abs:href");// storing link in node
					newnode.add(node1);

				}

			}
			System.out.println(links.size());
			System.out.println(newnode.size());
			data = new String[newnode.size()][2];// initializing table data
			task = new Task();
			task.start();// start with task

		}

		if (e.getSource() == button1) {
			String s[] = pages.getText().split("\\r?\\n");// storing content of
															// text area
			ArrayList<String> arrList = new ArrayList<>(Arrays.asList(s));
			regex.setText(CommonLink.commonString(arrList));// getting regex and
															// writing on text
															// field

		}
		if (e.getSource() == apply) {
			int reply = JOptionPane.showConfirmDialog(null, "Are you sure you want apply regex?", "Apply regex",
					JOptionPane.YES_NO_OPTION);
			if (reply == JOptionPane.YES_OPTION) {
				coll1.update(new BasicDBObject("domainName", domain), // storing
																		// regex
																		// in
																		// domain
																		// collection
						new BasicDBObject("$set", new BasicDBObject("regex", regex.getText())));
				JOptionPane.showMessageDialog(sampleCrawlFrame, "Applied regex successfully");
				new DomainStatsPage().domainStatsPage();// redirecting to domain
				// stats page
				sampleCrawlFrame.dispose();

			}

		}
		if (e.getSource() == back) {
			int reply = JOptionPane.showConfirmDialog(null, "Are you sure you want to cancle?", "Back to dashboard",
					JOptionPane.YES_NO_OPTION);
			if (reply == JOptionPane.YES_OPTION) {
				new DomainStatsPage().domainStatsPage();// redirecting to domain
														// stats page
				sampleCrawlFrame.dispose();
			}

		}

	}

	/**
	 * This class includes progressbar, table and mouse listener
	 * 
	 * @author Sourabh
	 *
	 */
	private class Task extends Thread {

		public Task() {

		}

		/**
		 * This method add data into table
		 */
		public void run() {

			for (int i = 0; i < newnode.size(); i++) {
				final int j = i;
				final int progress = Math.round((i + 1) * 100 / newnode.size());

				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						progressBar.setValue(progress);// setting value to
														// progress bar
						System.out.println(j);
						System.out.println("link : " + newnode.get(j).url);
						System.out.println("text : " + newnode.get(j).text);
						data[j][0] = newnode.get(j).text;// adding text to table
						data[j][1] = newnode.get(j).url;// adding url to table

					}
				});

				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
				}

			}

			jt = new JTable(data, column);// initializing table
			sp = new JScrollPane(jt);// initializing scroll pane with table
			sp.setBounds(5, 300, 1350, 200);
			sampleCrawlFrame.add(sp);
			jt.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent evnt) {

					if (evnt.getClickCount() == 1) {
						int row = jt.rowAtPoint(evnt.getPoint());// getting row
																	// after
																	// click
						int col = jt.columnAtPoint(evnt.getPoint());// getting
																	// column
																	// after
																	// click
						TableCellEditor tableCellRenderer = jt.getCellEditor(row, col);// getting
																						// table
																						// cell
																						// renderer
																						// with
																						// row
																						// and
																						// column
						Component c = jt.prepareEditor(tableCellRenderer, row, col);// creating
																					// component
																					// for
																					// cell
						c.setEnabled(false);
						c.setBackground(Color.pink);
						String myString = newnode.get(row).url;
						StringSelection stringSelection = new StringSelection(myString);// getting
																						// clicked
																						// value
																						// on
																						// cell
						Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();// storing
																							// value
																							// of
																							// cell
																							// on
																							// clipboard
						clpbrd.setContents(stringSelection, null);
					}
				}
			});
		}

	}

}

/**
 * This class is for data structure for node
 * 
 * @author sourabh
 *
 */
class node {
	String text, url;
}