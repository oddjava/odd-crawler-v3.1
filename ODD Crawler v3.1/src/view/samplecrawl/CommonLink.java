package view.samplecrawl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class CommonLink {
    public static String commonString(ArrayList<String> ArrayListInput) {
        char[] smallArray;
        String commonStr = "";
        String smallStr = "";
        for (String str : ArrayListInput) {
            if (smallStr.length() >= str.length()) continue;
            smallStr = str;
        }
        String temp = "";
        char[] arrc = smallArray = smallStr.toCharArray();
        int n = arrc.length;
        int n2 = 0;
        while (n2 < n) {
            char charactor = arrc[n2];
            temp = String.valueOf(temp) + charactor;
            block2 : for (String CommonChar : ArrayListInput) {
                if (CommonChar.contains(temp)) continue;
                temp = String.valueOf(temp) + charactor;
                for (String CommonChar2 : ArrayListInput) {
                    if (CommonChar2.contains(temp)) continue;
                    temp = "";
                    break block2;
                }
            }
            if (temp != "" && temp.length() > commonStr.length()) {
                commonStr = temp;
            }
            ++n2;
        }
        return commonStr;
    }

    public static ArrayList<String> getarraylistfrom_file(File f) throws FileNotFoundException {
        ArrayList<String> list = new ArrayList<String>();
        Scanner scan = new Scanner(f);
        while (scan.hasNext()) {
            list.add(scan.next());
        }
        scan.close();
        return list;
    }

    
}