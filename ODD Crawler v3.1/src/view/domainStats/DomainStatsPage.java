package view.domainStats;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.poi.hwpf.usermodel.BorderCode;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;

import addDomainLogic.MongoConnectionUI;
import dateTime.Date;
import mongoDBConnection.GetConnection;
import view.frameSetting.JFrameSettings;
import view.login.LoginPage;
import view.samplecrawl.SampleCrawl;
import view.settings.ConnectMongoDB;
import view.settings.DomainName;
import view.settings.SeedUrl;
import view.settings.SimpleExcelReaderExample;
import view.topPanel.Navigate;
import view.topPanel.TopPanel;

class Data {
	String domainName, regex, last7, total;
}
/**
 * This class tells statistics about domain
 * @author ajit
 *
 */
public class DomainStatsPage extends JPanel implements ActionListener{

	/*
	 * public void Page() { JFrame frame = new JFrame("JFrame Example");
	 * 
	 * JPanel panel = new JPanel();
	 * 
	 * panel.setLayout(new FlowLayout());
	 * 
	 * JLabel label = new JLabel("This is a label!");
	 * 
	 * JButton button = new JButton();
	 * 
	 * button.setText("Press me");
	 * 
	 * panel.add(label);
	 * 
	 * panel.add(button); frame.add(panel);
	 * 
	 * frame.setSize(300, 300);
	 * 
	 * frame.setLocationRelativeTo(null);
	 * 
	 * frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 * 
	 * frame.setVisible(true); }
	 */

	public DomainStatsPage() {
		// TODO Auto-generated constructor stub
	}

	JFrame mainFrame; // JFrame reference
	JButton addDomainButton; // JButton reference for 'Add Domain Button'
	JTable table;
	JScrollPane scrollbar;
	String[] header = { "Domain Name", "Regex Status", "Last 7 days count", "Total" }; // Table
																						// header
																						// values
	JLabel numOfSeedUrlLabel, addedDateLabel, date1Label, date2Label, date3Label, date4Label, date5Label, date6Label,
			date7Label;

	JLabel numOfSeedUrlValueLabel, addedDateValueLabel, date1ValueLabel, date2ValueLabel, date3ValueLabel,
			date4ValueLabel, date5ValueLabel, date6ValueLabel, date7ValueLabel;

	JLabel seedUrlLabel;

	List<String> seedUrl = new ArrayList<String>();
/**
 * This method 
 */
	public void domainStatsPage() {
		mainFrame = JFrameSettings.settings("Domain Stats Page");
		mainFrame.setJMenuBar(TopPanel.getPanel()); // Top panel added to Frame
		
		final JPanel seedPanel = new JPanel();
		seedPanel.setLayout(null);
		//seedPanel.setBounds(700, 310, 500, 500);
		seedPanel.setPreferredSize(new Dimension(700, 310));
		seedPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		mainFrame.add(seedPanel);
		JScrollPane seedScrollPane = new JScrollPane(seedPanel);
		seedScrollPane.setBounds(600, 310, 700, 310);
		seedScrollPane.setPreferredSize(new Dimension(500, 100));
		mainFrame.add(seedScrollPane);
		
		
		addDomainButton = new JButton("Add new Domain");
		addDomainButton.setBounds(10, 10, 150, 40);
		//addDomainButton.addActionListener(new MongoConnectionUI());
		addDomainButton.addActionListener(this);
		mainFrame.add(addDomainButton);

		numOfSeedUrlLabel = new JLabel("Number of Seed Urls");
		numOfSeedUrlLabel.setBounds(100, 340, 200, 50);
		mainFrame.add(numOfSeedUrlLabel);

		numOfSeedUrlValueLabel = new JLabel();

		addedDateLabel = new JLabel("Added Date");
		addedDateLabel.setBounds(100, 370, 200, 50);
		mainFrame.add(addedDateLabel);

		addedDateValueLabel = new JLabel();

		date1Label = new JLabel(Date.getDate(7));
		date1Label.setBounds(100, 400, 200, 50);
		mainFrame.add(date1Label);

		date1ValueLabel = new JLabel();

		date2Label = new JLabel(Date.getDate(6));
		date2Label.setBounds(100, 430, 200, 50);
		mainFrame.add(date2Label);

		date2ValueLabel = new JLabel();

		date3Label = new JLabel(Date.getDate(5));
		date3Label.setBounds(100, 460, 200, 50);
		mainFrame.add(date3Label);

		date3ValueLabel = new JLabel();

		date4Label = new JLabel(Date.getDate(4));
		date4Label.setBounds(100, 490, 200, 50);
		mainFrame.add(date4Label);

		date4ValueLabel = new JLabel();

		date5Label = new JLabel(Date.getDate(3));
		date5Label.setBounds(100, 520, 200, 50);
		mainFrame.add(date5Label);

		date5ValueLabel = new JLabel();

		date6Label = new JLabel(Date.getDate(2));
		date6Label.setBounds(100, 550, 200, 50);
		mainFrame.add(date6Label);

		date6ValueLabel = new JLabel();

		date7Label = new JLabel(Date.getDate(1));
		date7Label.setBounds(100, 580, 200, 50);
		mainFrame.add(date7Label);

		date7ValueLabel = new JLabel();

		/*
		 * seedUrl.add("www.dice.com/?q=telecommute");
		 * seedUrl.add("www.dice.com/?q=freelance");
		 * seedUrl.add("www.dice.com/?q=work+from+home");
		 */
		/*int y = 310;
		for (int i = 0; i < seedUrl.size(); i++) {
			seedUrlLabel = new JLabel(seedUrl.get(i));
			y = y + 30;
			seedUrlLabel.setBounds(700, y, 500, 50);
			mainFrame.add(seedUrlLabel);
		}*/

		List<String> domain = new ArrayList<>();
		List<String> regex = new ArrayList<>();
		List<String> last7 = new ArrayList<>();
		List<String> total = new ArrayList<>();

		Data dataObject = new Data();
		DBCollection coll = GetConnection.connect("domain");
		BasicDBObject query = new BasicDBObject();
		query.put("systemName", LoginPage.gettypename());
		DBCursor cursor = coll.find(query); // If we don not have query then keep
										// brackets blank. It will fetch all
										// records
		while (cursor.hasNext()) {
			// domain.add(new DomainStatsPage(cursor.next().get("domainName"),
			// cursor.next().get("regex"), 0,0));
			/*
			 * dataObject.domainName=(String) cursor.next().get("domainName");
			 * dataObject.regex=(String) cursor.next().get("regex");
			 * dataObject.last7="0"; dataObject.total="0";
			 * domain.add(dataObject);
			 */
			DBObject object = cursor.next();
			domain.add((String) object.get("domainName"));
			regex.add((String) object.get("regex"));
			last7.add("0");
			total.add("0");
		}

		String[][] data = new String[domain.size()][4];

		for (int i = 0; i < domain.size(); i++) {
			data[i][0] = (String) domain.get(i);
			data[i][1] = (String) regex.get(i);
			data[i][2] = (String) last7.get(i);
			data[i][3] = (String) total.get(i);
		}
		TableModel model = new DefaultTableModel(data, header) {
			public boolean isCellEditable(int row, int column) {
				return false;// This causes all cells to be not editable
			}
		};
		table = new JTable(model);

		scrollbar = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		scrollbar.setBounds(0, 100, 1370, 200);
		mainFrame.add(scrollbar);

		table.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				int row = table.rowAtPoint(evt.getPoint());
				int col = table.columnAtPoint(evt.getPoint());
				if (row >= 0 && col == 0) {
					if (evt.getClickCount() == 2) {
						//JOptionPane.showMessageDialog(null, "You double clicked on " + table.getValueAt(row, col)); // Here
						new SampleCrawl(table.getValueAt(row, col).toString());																						// we
						mainFrame.dispose();																							// we
																													// get
																													// the
																													// domain
																													// name
																													// which
																													// is
																													// double
																													// clicked
																													// by
																													// user.
					}
				}
				if (row >= 0 && col == 0) {
					// JOptionPane.showMessageDialog(null, "You clicked on
					// "+table.getValueAt(row, col)); //Here we get the domain
					// name which is selected by user.
					
					
					seedUrl.clear();
					DBCollection collection=GetConnection.connect("SeedUrl");
					BasicDBObject query = new BasicDBObject();
					query.put("domainUrl", table.getValueAt(row, col));	//parameterized query. here we provide the condition
					DBCursor cursor = collection.find(query);	//If we don not have query then keep brackets blank. It will fetch all records
					while (cursor.hasNext()) {
					   // System.out.println(cursor.next());
						DBObject object = cursor.next();
					    System.out.println(object.get("url"));
					    seedUrl.add(object.get("url").toString());
					}
					seedPanel.removeAll();
					
					seedPanel.setPreferredSize(new Dimension(700, 310));
					int y = 10;
					int panelY=310;
					for (int i = 0; i < seedUrl.size(); i++) {
						seedPanel.setPreferredSize(new Dimension(700, panelY));
						seedUrlLabel = new JLabel(seedUrl.get(i));
						y = y + 30;
						panelY = panelY + 30;
						//seedUrlLabel.setBounds(700, y, 500, 50);
						seedUrlLabel.setBounds(0, y, 5000, 50);
						seedPanel.add(seedUrlLabel);
					}

					String addedDate = "Yet to fetch";
					query.put("domainUrl", table.getValueAt(row, col));	//parameterized query. here we provide the condition
					cursor = collection.find(query);	//If we don not have query then keep brackets blank. It will fetch all records
					while (cursor.hasNext()) {
					   // System.out.println(cursor.next());
						DBObject object = cursor.next();
					    //System.out.println(object.get("addedDate"));
					    addedDate=object.get("addedDate").toString();
					}
					
					numOfSeedUrlValueLabel.setText(String.valueOf(seedUrl.size()));
					numOfSeedUrlValueLabel.setBounds(250, 340, 500, 50);
					mainFrame.add(numOfSeedUrlValueLabel);
					
					addedDateValueLabel.setText(addedDate);
					addedDateValueLabel.setBounds(250, 370, 500, 50);
					mainFrame.add(addedDateValueLabel);

					collection=GetConnection.connect("crawlData");
					date1ValueLabel.setText(String.valueOf(collection.getCount(new BasicDBObject("domainLink", table.getValueAt(row, col)).append("systemDate", date1Label.getText()))));
					date1ValueLabel.setBounds(250, 400, 500, 50);
					mainFrame.add(date1ValueLabel);

					date2ValueLabel.setText(String.valueOf(collection.getCount(new BasicDBObject("domainLink", table.getValueAt(row, col)).append("systemDate", date2Label.getText()))));
					date2ValueLabel.setBounds(250, 430, 500, 50);
					mainFrame.add(date2ValueLabel);

					date3ValueLabel.setText(String.valueOf(collection.getCount(new BasicDBObject("domainLink", table.getValueAt(row, col)).append("systemDate", date3Label.getText()))));
					date3ValueLabel.setBounds(250, 460, 500, 50);
					mainFrame.add(date3ValueLabel);

					date4ValueLabel.setText(String.valueOf(collection.getCount(new BasicDBObject("domainLink", table.getValueAt(row, col)).append("systemDate", date4Label.getText()))));
					date4ValueLabel.setBounds(250, 490, 500, 50);
					mainFrame.add(date4ValueLabel);

					date5ValueLabel.setText(String.valueOf(collection.getCount(new BasicDBObject("domainLink", table.getValueAt(row, col)).append("systemDate", date5Label.getText()))));
					date5ValueLabel.setBounds(250, 520, 500, 50);
					mainFrame.add(date5ValueLabel);

					date6ValueLabel.setText(String.valueOf(collection.getCount(new BasicDBObject("domainLink", table.getValueAt(row, col)).append("systemDate", date6Label.getText()))));
					date6ValueLabel.setBounds(250, 550, 500, 50);
					mainFrame.add(date6ValueLabel);

					date7ValueLabel.setText(String.valueOf(collection.getCount(new BasicDBObject("domainLink", table.getValueAt(row, col)).append("systemDate", date7Label.getText()))));
					date7ValueLabel.setBounds(250, 580, 500, 50);
					mainFrame.add(date7ValueLabel);

					mainFrame.repaint();
				}
			}
		});

		Navigate navigate = new Navigate(mainFrame);
		TopPanel.dashboard.addMenuListener(navigate);
		TopPanel.settings.addMenuListener(navigate);
		TopPanel.logout.addMenuListener(navigate);
		TopPanel.training.addMenuListener(navigate);
		TopPanel.verification.addMenuListener(navigate);
		TopPanel.domainStats.addActionListener(navigate);
		TopPanel.systemStats.addActionListener(navigate);
	}

	/**
	 * This method will fetch table contents from database
	 */
	public void tableValues() {

	}

	public static void main(String s[]) {
		// new DomainStatsPage().Page();
		new DomainStatsPage().domainStatsPage();
	}

	/*
	 * public void paint(Graphics g) { Graphics2D g2 = (Graphics2D) g;
	 * g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	 * RenderingHints.VALUE_ANTIALIAS_ON); g2.setPaint(Color.RED); int x = 5;
	 * int y = 7; g2.draw(new Line2D.Double(x, y, 200, 200));
	 * g2.drawString("Line", x, 250); g2.drawLine(350, 580, 450, 580); }
	 */

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		System.out.println("In paint!!");
		Graphics2D g2 = (Graphics2D) g;
		Line2D lin = new Line2D.Float(100, 100, 250, 260);
		g2.draw(lin);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		// btn_uploadFile_SeedURl event code
		if (event.getSource() == addDomainButton) {
			String sysDate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date()).toString();

			File selectedFile = null;
			SimpleExcelReaderExample reader = null;
			List<SeedUrl> seedUrlData = null;
			JOptionPane.showMessageDialog(this,
					"Please Add Excel File which is in proper format.\nDo not add in url column Dublicate records in Excel File,\nIf there is any dublicate records,\nthen please remove it using Excel tool \"remove Duplicates\"");
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel Files", "xlsx");
			fileChooser.setFileFilter(filter);
			int result = fileChooser.showOpenDialog(this);
			if (result == JFileChooser.APPROVE_OPTION) {
				selectedFile = fileChooser.getSelectedFile();
				System.out.println("Selected file: " + selectedFile.getAbsolutePath());
				reader = new SimpleExcelReaderExample();
				try {
					seedUrlData = reader.readBooksFromExcelFile(selectedFile.getAbsolutePath());
					MongoDatabase db = ConnectMongoDB.connectToMongoDB();

					/*
					 * ListIndexesIterable<Document>
					 * ind=db.getCollection("SeedUrl").listIndexes();
					 * MongoCursor<Document> cur=ind.iterator();
					 * while(cur.hasNext()) { System.out.println(cur.next().); }
					 */

					// it will create unique indexes
					db.getCollection("SeedUrl").createIndex(new Document("url", 1), new IndexOptions().unique(true));
					db.getCollection("domain").createIndex(new Document("domainName", 1),
							new IndexOptions().unique(true));

					Iterator<SeedUrl> data = seedUrlData.iterator();

					while (data.hasNext()) {
						SeedUrl temp = data.next();

						System.out.println(
								"Domain Type -  " + temp.getSubDomainOrMaindomain() + "  Url -  " + temp.getUrl());

						try {
							// here data will be added in database
							db.getCollection("SeedUrl")
									.insertOne(new Document("url", temp.getUrl())
											.append("domainUrl",
													DomainName.getDomainName(temp.getSubDomainOrMaindomain(),
															temp.getUrl()))
											.append("moduleName", temp.getModuleName())
											.append("systemName", temp.getSystemName()).append("addedDate", sysDate).// getFrom
																														// excel
																														// file
							append("subdomain/maindomain", temp.getSubDomainOrMaindomain()).append("bucketNumber", 0)
											.append("nextCrawlDate", "2016-03-19").append("type", temp.getType()));

							db.getCollection("domain")
									.insertOne(
											new Document("domainName",
													DomainName.getDomainName(temp.getSubDomainOrMaindomain(),
															temp.getUrl())).append("keyword", temp.getKeyWords())
																	.append("description", temp.getDescription())
																	.append("regex", temp.getRegx())
																	.append("systemName", temp.getSystemName())
																	.append("addedDate", sysDate));
						} catch (MongoWriteException e) {
							data.next();
							System.out.println("Find duplicate value");
							// e.printStackTrace();
						}

					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (OpenXML4JException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				JOptionPane.showMessageDialog(this,
						"You did not selected file please select Excel file for upload data");

			}

			System.out.println("Upload Data Succesfullly");

		}
	}
}
