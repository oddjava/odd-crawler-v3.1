package view.topPanel;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class TopPanel {
	public static JMenuBar menu;
	public static JMenu dashboard,stats,training,verification,settings,logout;
	public static JMenuItem systemStats,domainStats;
	
	public static JMenuBar getPanel(){
		//JPanel panel=new JPanel();
		menu = new JMenuBar();
		dashboard = new JMenu("Dashboard");
		stats = new JMenu("Stats");
		training = new JMenu("Training Sets");
		verification = new JMenu("Verification");
		settings = new JMenu("Settings");
		logout = new JMenu("Logout");
		systemStats = new JMenuItem("System Stats");
		domainStats = new JMenuItem("Domain Stats");
		
		stats.add(systemStats);
		stats.add(domainStats);
		menu.add(dashboard);
		menu.add(stats);
		menu.add(training);
		menu.add(verification);
		menu.add(settings);
		menu.add(logout);
		menu.setBounds(0, 0, 450, 25);
		//panel.add(menu);
		return menu;

		}
}
