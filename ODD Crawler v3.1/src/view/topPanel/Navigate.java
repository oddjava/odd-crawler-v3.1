package view.topPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import menu.MenuBar;
import view.SystemStats.systemStatsPage;
import view.crawlerPage.CrawlerPage;
import view.domainStats.DomainStatsPage;
import view.frameSetting.JFrameSettings;
import view.login.LoginPage;
import view.settings.SettingPage;
//import view.settings.SettingsPage;
import view.trainingSetPage.TrainingSetPage;
import view.verification.VerificationPage;

public class Navigate implements MenuListener,ActionListener{

	JFrame mainFrame;
	
	JMenuBar menu;
	public Navigate(JFrame mainFrame) {
		// TODO Auto-generated constructor stub
		this.mainFrame=mainFrame;
	}
	@Override
	public void menuCanceled(MenuEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("Canceled");
	}

	@Override
	public void menuDeselected(MenuEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("Deselected");
	}

	@Override
	public void menuSelected(MenuEvent e) {
		// TODO Auto-generated method stub
		//mainFrame.dispose();
		//System.out.println("Selected");
		mainFrame.dispose();
		System.out.println(((JMenu)e.getSource()).getText());
		String frameName=((JMenu)e.getSource()).getText();
		
		if(frameName.contentEquals("Dashboard"))
		{
			new CrawlerPage();
		}
		if(frameName.contentEquals("Logout"))
		{
			new LoginPage();
		}
		if(frameName.contentEquals("Training Sets"))
		{
			try {
				new TrainingSetPage();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if(frameName.contentEquals("Verification"))
		{
			new VerificationPage();
		}
		if(frameName.contentEquals("Settings"))
		{
			new SettingPage();
		}
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		mainFrame.dispose();
		if(e.getSource().equals(TopPanel.domainStats))
		{
			System.out.println("Domain Stats page");
			new DomainStatsPage().domainStatsPage();;
		}
		if(e.getSource().equals(TopPanel.systemStats))
		{
			System.out.println("System Stats page");
			new systemStatsPage().SystemStatsGUI();
		}
	}
}


/*public class sample
{
	public static void main(String[] args) {
		JFrame main=JFrameSettings.settings("sample");
		JMenuBar menu=TopPanel.getPanel();
		main.setJMenuBar(menu);
		TopPanel.dashboard.addMenuListener(new Navigate(main));
		TopPanel.logout.addMenuListener(new Navigate(main));
	}
}*/
