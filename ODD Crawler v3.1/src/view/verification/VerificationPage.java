package view.verification;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.gravity.goose.Article;
import com.gravity.goose.Configuration;
import com.gravity.goose.Goose;

import Jsoup.JsoupConnect;
import extractor.boiler.BoilerPipeMainText;
import extractor.imageExtractor.GooseImageExtraction;
import extractor.ner.StanFordNER;
import view.frameSetting.JFrameSettings;
import view.topPanel.Navigate;
import view.topPanel.TopPanel;

/**
 * This code is to verify current given links
 * 
 * @author Sourabh
 *
 */
public class VerificationPage extends JFrame implements ActionListener {
	ArrayList<Datanode> newnode = new ArrayList<>();
	JLabel lbl_MetaDescription = new JLabel("Meta Description-");
	JLabel lbl_Keyword = new JLabel("Meta Keyword-");
	JLabel lbl_Image = new JLabel("Image-");
	JLabel lbl_Name = new JLabel("Name -");
	JLabel lbl_Category = new JLabel("Category-");
	JLabel lbl_NLP = new JLabel("NLP  -");
	JLabel lbl_Location = new JLabel("Location -");
	JLabel lbl_FetchedDate = new JLabel("Fetched Date -");
	JLabel lbl_MainContent = new JLabel("Main Content -");
	JLabel lbl_HeaderType = new JLabel("Header Type -");
	JLabel lbl_Status = new JLabel("Status-");
	JFrame verification = JFrameSettings.settings("Verification Page");
	JMenuBar newMenuBar = TopPanel.getPanel();
	JTextArea textarea = new JTextArea();
	JButton verify = new JButton("Verify");
	JPanel panel = new JPanel();
	JPanel imgpanel = new JPanel();
	JLabel label = new JLabel();
	JTable jt;
	int n = 0;
	JScrollPane sp;
	String[][] data = { { "", "", "", "", "", "", "", "", "", "" } };
	String column[] = { "Link", "Description", "Keyword", "Image", "Name", "Location", "Date", "Category", "NLP",
			"Content" };
	Document doc;

	/**
	 * This pages creates the frame and adds nevigation to it
	 */
	public VerificationPage() {
		panel.setBounds(230, 350, 500, 650);

		verification.setJMenuBar(newMenuBar);

		textarea.setBounds(50, 50, 500, 100);
		verification.add(textarea);

		verify.setBounds(600, 100, 100, 30);
		verification.add(verify);
		verify.addActionListener(this);

		verification.repaint();
		Navigate navigate = new Navigate(verification);
		TopPanel.dashboard.addMenuListener(navigate);
		TopPanel.settings.addMenuListener(navigate);
		TopPanel.logout.addMenuListener(navigate);
		TopPanel.training.addMenuListener(navigate);
		TopPanel.verification.addMenuListener(navigate);
		TopPanel.domainStats.addActionListener(navigate);
		TopPanel.systemStats.addActionListener(navigate);
	}

	public static void main(String[] args) {
		new VerificationPage();
	}

	@Override
	/**
	 * This is action listener to verify button. It takes links entered in text
	 * area in an arraylist. Passes arraylist to regex finding method. Crawl
	 * link as per regex found. Adds table. And also add mouse listener to table
	 * and displays data for clicked row.
	 */
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == verify) {
			newnode.clear();// clears previous data in arraylist
			if (n > 0) {// condition to check crawling is done before or not
				verification.remove(sp);// removes the scroll pane from frame
				verification.repaint();
			}
			n++;
			String arr[] = textarea.getText().split("\\r?\\n");// takes text
																// area content
																// in array
			for (int i = 0; i < arr.length; i++) {
				Datanode newnode1 = new Datanode();// create object for datanode
				newnode1.link = arr[i];// adds link to link field in node
				newnode.add(newnode1);
			}
			for (int r = 0; r < newnode.size(); r++) {
				System.out.println(newnode.get(r).link);// displays all links

			}

			try {
				filldata();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			filltable();

			lbl_MetaDescription.setBounds(100, 360, 100, 20);
			verification.add(lbl_MetaDescription);

			lbl_Keyword.setBounds(100, 390, 100, 20);
			verification.add(lbl_Keyword);

			lbl_Image.setBounds(100, 420, 100, 20);
			verification.add(lbl_Image);

			lbl_Name.setBounds(100, 450, 100, 20);
			verification.add(lbl_Name);

			lbl_Category.setBounds(100, 480, 100, 20);
			verification.add(lbl_Category);

			lbl_NLP.setBounds(100, 510, 100, 20);
			verification.add(lbl_NLP);

			lbl_Location.setBounds(100, 540, 100, 20);
			verification.add(lbl_Location);

			lbl_FetchedDate.setBounds(100, 570, 100, 20);
			verification.add(lbl_FetchedDate);

			lbl_MainContent.setBounds(100, 600, 100, 20);
			verification.add(lbl_MainContent);

			lbl_HeaderType.setBounds(100, 630, 100, 20);
			verification.add(lbl_HeaderType);

			lbl_Status.setBounds(100, 660, 100, 20);
			verification.add(lbl_Status);

			jt = new JTable(data, column);// creats the jtable
			sp = new JScrollPane(jt);// creates scroll pane for jtable
			sp.setBounds(5, 180, 1350, 150);
			verification.add(sp);// adds scrollpane on frame
			jt.setEnabled(false);
			jt.addMouseListener(new MouseAdapter() {// mouse listener to table
				public void mouseClicked(MouseEvent evnt) {
					panel.removeAll();// removes previous data from panel
					if (evnt.getClickCount() == 1) {

						int count = jt.rowAtPoint(evnt.getPoint());// takes the
																	// row index
																	// in count
																	// after
																	// mouse
																	// click

						JLabel d[] = new JLabel[11];

						d[0] = new JLabel(newnode.get(count).description);
						d[0].setBounds(0, 0, 500, 30);

						d[1] = new JLabel(newnode.get(count).keyword);
						d[1].setBounds(0, 30, 500, 30);

						d[2] = new JLabel(newnode.get(count).image);
						d[2].setBounds(0, 60, 500, 30);

						d[3] = new JLabel(newnode.get(count).name);
						d[3].setBounds(0, 90, 500, 30);

						d[4] = new JLabel(newnode.get(count).category);
						d[4].setBounds(0, 120, 500, 30);

						d[5] = new JLabel(newnode.get(count).nlp);
						d[5].setBounds(0, 150, 500, 30);

						d[6] = new JLabel(newnode.get(count).location);
						d[6].setBounds(0, 180, 500, 30);

						d[7] = new JLabel(newnode.get(count).date);
						d[7].setBounds(0, 210, 500, 30);

						d[8] = new JLabel(newnode.get(count).content);
						d[8].setBounds(0, 240, 500, 30);

						d[9] = new JLabel(newnode.get(count).headertype);
						d[9].setBounds(0, 270, 500, 30);

						d[10] = new JLabel(newnode.get(count).status);
						d[10].setBounds(0, 300, 500, 30);
						for (int p = 0; p < 11; p++)
							panel.add(d[p]);
						verification.add(panel);
						
						imgpanel.setBounds(1000, 350, 300, 300);
						imgpanel.setLayout(null);
						BufferedImage img = null;
						System.out.println(newnode.get(count).imagesrc);
						try {
							img = ImageIO.read(new File("./Images/" + newnode.get(count).imagesrc));// takes
																									// image
																									// path
																									// from
																									// node
						} catch (IOException e) {
						}
						Image scaledImage = img.getScaledInstance(imgpanel.getWidth(), imgpanel.getHeight(),
								Image.SCALE_SMOOTH);// creates image
						ImageIcon background = new ImageIcon(scaledImage);// creates
																			// icon
																			// for
																			// image
					
						imgpanel.add(label);
						label.setBounds(0, 0, 300, 300);
						label.setIcon(background);// setting icon on label
						imgpanel.remove(label);

						imgpanel.add(label);// adding label containing image
											// icon
						verification.add(imgpanel);
						

						verification.repaint();
					

					}
				}
			});

		}

	}

	/**
	 * This method performs crawling the links one by one and gets its info by
	 * jsoup, goose, boilerpipe, ner.
	 */
	void filldata() {
		for (int j = 0; j < newnode.size(); j++) {
			try {
				String present = newnode.get(j).link;
				System.out.println(newnode.get(j).link);
				doc = Jsoup.connect(present).timeout(500000).get();// connection
																	// to jsoup
																	// for
																	// present
																	// link
				newnode.get(j).keyword = doc.select("meta[name=keywords]").first().attr("content");// takes
																									// all
																									// keyword
																									// for
																									// link
				newnode.get(j).description = doc.select("meta[name=DESCRIPTION]").first().attr("content");// takes
																											// all
																											// description
																											// for
																											// link
				newnode.get(j).status = JsoupConnect.getHeader(present);// calling
																		// getHeader()
																		// for
																		// getting
																		// the
																		// header
																		// size
				newnode.get(j).headertype = JsoupConnect.getStatus(present);// calling
																			// getStatus()
																			// for
																			// getting
																			// hit
																			// status
																			// for
																			// link
				System.out.println(newnode.get(j).keyword + " " + newnode.get(j).description + " "
						+ newnode.get(j).status + " " + newnode.get(j).headertype);
				URL url = new URL(present);// creating url for link
				BoilerPipeMainText bpmt = new BoilerPipeMainText(url);// passing
																		// url
																		// to
																		// boiler
																		// pipe
				StanFordNER stanford = new StanFordNER(bpmt.process());// passing
																		// main
																		// text
																		// from
																		// boiler
																		// pipe
																		// to
																		// ner
				HashMap hm = stanford.getDataForAllClasses();// getting hashmap
																// object from
																// ner
				String date = hm.get("date").toString();// getting date from ner
				String location = hm.get("location").toString();// getting
																// location from
																// ner
				String name = hm.get("person").toString();// getting person name
															// from ner

				System.out.println(newnode.get(j).keyword + " " + newnode.get(j).description + " "
						+ newnode.get(j).status + " " + newnode.get(j).headertype);

				if (date.length() > 2)
					newnode.get(j).date = date.substring(1, date.length() - 1);
				else
					newnode.get(j).date = "-";

				if (location.length() > 2)
					newnode.get(j).location = location.substring(1, location.length() - 1);
				else
					newnode.get(j).location = "-";

				if (name.length() > 2)
					newnode.get(j).name = name.substring(1, name.length() - 1);
				else
					newnode.get(j).name = "-";

				newnode.get(j).content = bpmt.process();// getting main text
														// from boiler pipe

				newnode.get(j).image = ImageValue(j);// getting whether image is
														// present or not from
														// ImageValue()
				newnode.get(j).category = "-";
				newnode.get(j).nlp = "-";
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	/**
	 * This method performs goose operation
	 * 
	 * @param s
	 *            index for present link in arraylist
	 * @return whether image found or not
	 */

	String ImageValue(int s) {
		Goose goose = new Goose(new Configuration());// creating goose object
		Article article;
		article = goose.extractContent(newnode.get(s).link);// taking object by
															// passing link
		String src = article.topImage().imageSrc();// getting src for image

		if (src == "") {
			newnode.get(s).imagesrc = "no.jpg";// default image for no image
			return "NO";
		} else {
			GooseImageExtraction newgoose = new GooseImageExtraction();
			newgoose.ImageExtraction(newnode.get(s).link);
			int indexname = src.lastIndexOf("/");
			if (indexname == src.length()) {
				src = src.substring(1, indexname);
			}
			indexname = src.lastIndexOf("/");
			newnode.get(s).imagesrc = src.substring(indexname+1, src.length());// setting
																				// name
																				// for
																				// image
			return "YES";
		}

	}

	/**
	 * Fill table data comparing arraylist components
	 */
	void filltable() {
		data = new String[newnode.size()][10];// creats the data for table
												// for required size
		for (int k = 0; k < newnode.size(); k++) {// filling all table content
													// comparing arraylist's
													// data
			data[k][0] = newnode.get(k).link;
			if (newnode.get(k).description.length() > 0) {
				data[k][1] = "Y";
			} else {

				data[k][1] = "N";
			}
			if (newnode.get(k).content.length() > 0) {
				data[k][9] = "Y";
			} else {

				data[k][9] = "N";
			}

			if (newnode.get(k).keyword.length() > 0) {
				data[k][2] = "Y";
			} else {

				data[k][2] = "N";
			}

			if (newnode.get(k).name.length() > 1) {
				data[k][4] = "Y";
			} else {

				data[k][4] = "N";
			}

			if (newnode.get(k).location.length() > 1) {
				data[k][5] = "Y";
			} else {

				data[k][5] = "N";
			}

			if (newnode.get(k).date.length() > 1) {
				data[k][6] = "Y";
			} else {

				data[k][6] = "N";
			}
			if (newnode.get(k).image == "NO") {
				data[k][3] = "N";
			} else {
				data[k][3] = "Y";
			}

			if (newnode.get(k).category.length() > 1) {
				data[k][7] = "Y";
			} else {

				data[k][7] = "N";
			}

			if (newnode.get(k).nlp.length() > 1) {
				data[k][8] = "Y";
			} else {

				data[k][8] = "N";
			}

		}

	}
}

/**
 * This class is for creating data structure for single node for each link
 * 
 * @author Sourabh
 *
 */
class Datanode {
	String link, description, keyword, image, name, category, nlp, location, date, content, headertype, status,
			imagesrc;
}
