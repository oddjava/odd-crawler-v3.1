package view.SystemStats;

import menu.MenuBar;
import mongoDBConnection.GetConnection;
import view.frameSetting.JFrameSettings;
import view.topPanel.Navigate;
import view.topPanel.TopPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import javax.swing.*;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.toedter.calendar.JDateChooser;

import dateTime.Date;

/**
 * This page giving analysis for last seven days with respect to domain names
 * 
 * @author sourabh
 *
 */
public class systemStatsPage extends JFrame implements ActionListener {

	/**
	 * 
	 */
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private static final long serialVersionUID = 1L;
	private JFrame sysframe = JFrameSettings.settings("System Stats Page");
	private JLabel label1 = new JLabel("From");
	private JLabel label2 = new JLabel("To");
	private JButton ok = new JButton("Ok");
	private JButton reset = new JButton("Reset");
	JDateChooser dateChooser1 = new JDateChooser();
	JDateChooser dateChooser2 = new JDateChooser();
	DBCollection coll = GetConnection.connect("sampleCrawldata");
	List modules;
	JScrollPane sp;
	String[][] data;
	int n;

	public systemStatsPage() {
		SystemStatsGUI();
	}

	public static void main(String[] args) {
		new systemStatsPage();

	}

	/**
	 * This method adds all required data in table and showing on frame
	 */
	public void SystemStatsGUI() {
		modules = coll.distinct("moduleName");// getting all distinct module
												// names in list
		n = modules.size();
		data = new String[n][10];// initializing table

		for (int i = 0; i < n; i++) {
			data[i][0] = modules.get(i).toString();
			data[i][1] = String.valueOf(
					coll.getCount(new BasicDBObject("moduleName", data[i][0]).append("systemDate", Date.getDate(6))));
			data[i][2] = String.valueOf(
					coll.getCount(new BasicDBObject("moduleName", data[i][0]).append("systemDate", Date.getDate(5))));
			data[i][3] = String.valueOf(
					coll.getCount(new BasicDBObject("moduleName", data[i][0]).append("systemDate", Date.getDate(4))));
			data[i][4] = String.valueOf(
					coll.getCount(new BasicDBObject("moduleName", data[i][0]).append("systemDate", Date.getDate(3))));
			data[i][5] = String.valueOf(
					coll.getCount(new BasicDBObject("moduleName", data[i][0]).append("systemDate", Date.getDate(2))));
			data[i][6] = String.valueOf(
					coll.getCount(new BasicDBObject("moduleName", data[i][0]).append("systemDate", Date.getDate(1))));
			data[i][7] = String.valueOf(
					coll.getCount(new BasicDBObject("moduleName", data[i][0]).append("systemDate", Date.getDate(0))));
			data[i][8] = String.valueOf((Integer.parseInt(data[i][1]) + Integer.parseInt(data[i][2])
					+ Integer.parseInt(data[i][3]) + Integer.parseInt(data[i][4]) + Integer.parseInt(data[i][5])
					+ Integer.parseInt(data[i][6]) + Integer.parseInt(data[i][7])));
			data[i][9] = String.valueOf(coll.getCount(new BasicDBObject("moduleName", data[i][0])));

		}
		// mongoClient.close();
		JMenuBar newMenuBar = TopPanel.getPanel();
		sysframe.setJMenuBar(newMenuBar);

		label1.setBounds(50, 20, 50, 20);
		label2.setBounds(260, 20, 50, 20);
		sysframe.add(label1);
		sysframe.add(label2);

		sysframe.getContentPane().setLayout(null);

		dateChooser1.setBounds(100, 20, 100, 20);
		sysframe.getContentPane().add(dateChooser1);
		dateChooser2.setBounds(300, 20, 100, 20);
		sysframe.getContentPane().add(dateChooser2);
		java.util.Date model = new java.util.Date();

		dateChooser1.setDate(model);
		dateChooser2.setDate(model);

		ok.setBounds(450, 20, 100, 20);
		sysframe.add(ok);
		ok.addActionListener(this);

		reset.setBounds(600, 20, 100, 20);
		sysframe.add(reset);
		reset.addActionListener(this);

		String column[] = { "Crawler type", Date.getDate(6), Date.getDate(5), Date.getDate(4), Date.getDate(3),
				Date.getDate(2), Date.getDate(1), Date.getDate(0), "Total 7 Days", "Total" };
		JTable jt = new JTable(data, column);
		sysframe.add(jt);
		sp = new JScrollPane(jt);
		sp.setBounds(0, 50, 1350, 700);
		sysframe.add(sp);
		jt.setEnabled(false);

		System.out.println("In system stats");
		Navigate navigate = new Navigate(sysframe);
		TopPanel.dashboard.addMenuListener(navigate);
		TopPanel.settings.addMenuListener(navigate);
		TopPanel.logout.addMenuListener(navigate);
		TopPanel.training.addMenuListener(navigate);
		TopPanel.verification.addMenuListener(navigate);
		TopPanel.domainStats.addActionListener(navigate);
		TopPanel.systemStats.addActionListener(navigate);

	}

	@Override

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == ok) {
			if (dateChooser1.getDate().before(dateChooser2.getDate())) {

				long[] datacount = new long[n];
				Arrays.fill(datacount, 0);

				for (int j = 0; j < n; j++) {

					data[j][9] = String.valueOf(coll.getCount(new BasicDBObject("systemDate",
							new BasicDBObject("$gte", sdf.format(dateChooser1.getDate())).append("$lte",
									sdf.format(dateChooser2.getDate()))).append("moduleName",
											modules.get(j).toString())));
				}

				sysframe.add(sp);
			} else {
				JOptionPane.showMessageDialog(sysframe, "Please select correct dates");
			}
		}
		if (e.getSource() == reset) {

			for (int k = 0; k < n; k++) {

				data[k][9] = String.valueOf(coll.getCount(new BasicDBObject("moduleName", modules.get(k))));
			}

			sysframe.add(sp);

		}
	}
}