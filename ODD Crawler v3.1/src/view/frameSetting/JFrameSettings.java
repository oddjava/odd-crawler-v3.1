package view.frameSetting;

import javax.swing.JFrame;

public class JFrameSettings {

	public static JFrame settings(String title)
	{
		JFrame mainFrame = new JFrame(title);
		mainFrame.setSize(1600, 900);
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setLayout(null);
		mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		return mainFrame;
	}
}
