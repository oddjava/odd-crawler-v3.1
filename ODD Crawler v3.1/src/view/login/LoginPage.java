package view.login;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

import com.mongodb.BasicDBList;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import mongoDBConnection.GetConnection;
import view.crawlerPage.CrawlerPage;
import view.frameSetting.JFrameSettings;

/**
 * This page logins in the crawler. For this user should require username,
 * password and crawler type allotted.
 * 
 * @author Sourabh
 *
 */
public class LoginPage extends JFrame implements ActionListener {
	/**
	 * 
		 * 
		 */
	private static final long serialVersionUID = 1L;
	DBCollection coll1 = GetConnection.connect("settings");
	// String[] typelist = { "Complete System", "minus Image", "minus NLP",
	// "minus NLP Image", "RSS Feed Crawling" };
	private JFrame mainFrame;
	private JLabel label1 = new JLabel("Username:");
	private JLabel label2 = new JLabel("Password:");
	private JLabel label3 = new JLabel("Crawler type:");
	private JTextField username = new JTextField("");
	private JPasswordField password = new JPasswordField("");
	JComboBox<Object> combo;
	private JButton cancel = new JButton("Cancel");
	private JButton login = new JButton("Sign-in");
	static String ptype;
	static String puser;
	String ppass;

	/**
	 * This method calls to prepareGUI() method
	 */
	public LoginPage() {
		System.out.println("Login constructor");
		prepareGUI();
	}

	public static void main(String[] args) {
		new LoginPage();
	}

	/**
	 * This method creates the frame and adds required components
	 */
	private void prepareGUI() {

		ArrayList<String> set = new ArrayList<>();
		DBCursor cursor = coll1.find(); // creates cursor for collection of
										// settings page
		while (cursor.hasNext()) { // add all system names in set ie in combobox
			DBObject present = cursor.next();
			set.add(present.get("systemName").toString());

		}
		Object[] typelist = set.toArray();
		combo = new JComboBox<>(typelist);
		mainFrame = JFrameSettings.settings("Login Page");

		label1.setBounds(550, 100, 150, 20);
		mainFrame.add(label1);

		username.setBounds(700, 100, 150, 20);
		mainFrame.add(username);

		label2.setBounds(550, 150, 150, 20);
		mainFrame.add(label2);

		password.setBounds(700, 150, 150, 20);
		mainFrame.add(password);

		label3.setBounds(550, 200, 150, 20);
		mainFrame.add(label3);

		combo.setBounds(700, 200, 150, 20);
		mainFrame.add(combo);

		cancel.setBounds(550, 250, 100, 30);
		mainFrame.add(cancel);
		cancel.addActionListener(this);

		login.setBounds(750, 250, 100, 30);
		mainFrame.add(login);
		login.addActionListener(this);

	}

	/**
	 * This method checks for entered username, login and crawler type. If all
	 * credentials are correct it redirects to dashboard page.
	 */
	public void loginclick() {

		DBCollection coll = GetConnection.connect("dbdata");// creates
															// connection to
															// collection in
															// which credentials
															// are defined
		DBCursor cursor = coll.find();// creates cursor for credentials
										// collection
		puser = username.getText();// gets text from username text field
		ppass = password.getText();// gets text from password text field
		ptype = combo.getSelectedItem().toString();
		int i = 0;
		while (cursor.hasNext()) {
			DBObject present = cursor.next();
			String dbuser = present.get("username").toString();// stores
																// username
																// from
																// present
																// cursor from
																// database
			String dbpass = present.get("password").toString();// stores
																// password from
																// database
			BasicDBList dbtype = (BasicDBList) present.get("type");
			if (puser.equalsIgnoreCase(dbuser) && ppass.equalsIgnoreCase(dbpass) && dbtype.contains(ptype)) {// condition
																												// for
																												// all
																												// credentials
																												// are
																												// correct
				mainFrame.dispose();// disposes current frame
				new CrawlerPage();// call to dashboard page
				i++;
			} else if (puser.equalsIgnoreCase(dbuser) && ppass.equalsIgnoreCase(dbpass)) {// condition
																							// for
																							// if
																							// only
																							// username
																							// and
																							// password
																							// matches
																							// but
																							// crawler
																							// type
				JOptionPane.showMessageDialog(mainFrame, "Not authorised to use this crawler type");
				i++;
			}
		}
		if (i == 0) {// condition for username or password is not matches
			JOptionPane.showMessageDialog(mainFrame, "Username or Password incorrect");
		}
	}

	/**
	 * 
	 * @return crawlertype
	 * 
	 */
	public static String gettypename() {
		String newtype = ptype;
		return newtype;
	}

	/**
	 * 
	 * @return username
	 */
	public static String getusername() {
		String uname = puser;
		return uname;

	}

	/**
	 * This is the action listener for cancel and login button
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == cancel) {
			username.setText("");// makes username text field blank
			password.setText("");// makes password text field blank
			combo.setSelectedIndex(0);// makes combobox to point index 0
		} else if (e.getSource() == login) {
			if (username.getText().equals("") || password.getPassword().equals("")) {// condition
																						// for
																						// empty
																						// text
																						// fields
				JOptionPane.showMessageDialog(mainFrame, "Username or Password is empty");
			} else
				loginclick();// call to loginclick
		}

	}

}