package view.crawlerPage;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import view.frameSetting.JFrameSettings;
import view.login.LoginPage;
import view.topPanel.Navigate;
import view.topPanel.TopPanel;

import code.crawlerPage.CrawlerPageLogic;
import crawlLogic.CrawlingSeeds;
//import crawlLogic.seedData;
import dateTime.Date;
import dateTime.Time;
import domain.statusHeader;
import extractor.awsS3bucket.S3upload;
import extractor.boiler.BoilerPipeMainText;
import extractor.fevicon.DownloadFeviconImage;
import extractor.imageExtractor.GetByGoose;
import extractor.ner.StanFordNER;
import geoLocation.GeoLocation;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

import Jsoup.Crawl;
import Jsoup.MetaData;
import robots.*;
import apacheNLP.NLP;
import categorization.CategoryClassification;
import mongoDBConnection.GetConnection;
import removalofSpecial.RemoveSpecialChar;
import translation.Translation;

public class CrawlerPage extends JFrame implements ActionListener {
	/**
	 * 
	 */

	DBCollection crawldataDB = GetConnection.connect("crawldata");

	DBCollection sampleCrawlDataDB = GetConnection.connect("sampleCrawldata");

	DBCollection seedUrlDB = GetConnection.connect("SeedUrl");

	DBCollection domainDB = GetConnection.connect("domain");

	DBCollection nlpFileNameDB = GetConnection.connect("nlpFilename");

	DBCollection settingsDb = GetConnection.connect("settings");

	DBCollection nameStructureDB = GetConnection.connect("nameStructure");

	String nlp, ner, image, category, translate;
	public static Map<String, String> modules = new HashMap<>();
	private static final long serialVersionUID = 1L;
	public JFrame crawl = JFrameSettings.settings("DashBoard");
	private JButton start = new JButton("Start");
	private JButton schedule = new JButton("Schedule");
	private JLabel label1 = new JLabel("Prev Time:");
	private JLabel label2 = new JLabel("Next Time:");

	private JLabel time1 = new JLabel();
	private JLabel time2 = new JLabel();
	// public JProgressBar progressBar = new JProgressBar(0, 100);

	String link;
	String prev;
	String next;
	int n = 0;
	Document doc;
	Elements links;
	JScrollPane sp;
	ArrayList<Data> data = new ArrayList<>();
	Set paginationSeedUrl = new HashSet<>();

	JLabel linkLabel = new JLabel("Link");
	JLabel descriptionLabel = new JLabel("Meta Description");
	JLabel keywordLabel = new JLabel("Meta Keyword");
	JLabel contentLabel = new JLabel("Main Content");
	JLabel imageLabel = new JLabel("Image");
	JLabel nameLabel = new JLabel("Names");
	JLabel locationLabel = new JLabel("Location");
	JLabel fetchedDateLabel = new JLabel("Fetched Date");
	JLabel headerTypeLabel = new JLabel("Header Type");
	JLabel statusLabel = new JLabel("Status");

	JLabel linkLabelValues = new JLabel();
	JLabel descriptionLabelValues = new JLabel();
	JLabel keywordLabelValues = new JLabel();
	JLabel contentLabelValues = new JLabel();
	JLabel imageLabelValues = new JLabel();
	JLabel nameLabelValues = new JLabel();
	JLabel locationLabelValues = new JLabel();
	JLabel fetchedDateLabelValues = new JLabel();
	JLabel headerTypeLabelValues = new JLabel();
	JLabel statusLabelValues = new JLabel();

	ArrayList<CrawlingSeeds> seedUrl = new ArrayList<>();
	ArrayList<CrawlingSeeds> crawlingSeeds = new ArrayList<>();
	ArrayList<String> nameStructure = new ArrayList<>();

	// Task task;
	int progressCalc = 0;
	int prevProgress = 0;

	public static void main(String[] args) {
		new CrawlerPage();

	}

	public CrawlerPage() {

		BasicDBObject settingsQuery = new BasicDBObject();
		JMenuBar newMenuBar = TopPanel.getPanel();
		crawl.setJMenuBar(newMenuBar);

		// System.out.println(LoginPage.gettypename());
		start.setBounds(40, 40, 100, 30);
		crawl.add(start);

		String timeStamp = Time.getTime();
		BasicDBObject newDocument = new BasicDBObject();
		newDocument.append("$set", new BasicDBObject().append("prev", timeStamp));
		BasicDBObject searchQuery = new BasicDBObject().append("systemName", LoginPage.gettypename());
		settingsDb.update(searchQuery, newDocument);
		// System.out.println("time updated" + timeStamp);

		start.addActionListener(this);

		// System.out.println("Before adding progress on frame");
		/*
		 * progressBar = new JProgressBar(0, 100); progressBar.setValue(0);
		 * progressBar.setStringPainted(true); progressBar.setBounds(200, 40,
		 * 500, 30); crawl.add(progressBar);
		 */
		// System.out.println("After adding progress on frame");

		schedule.setBounds(750, 40, 100, 30);
		crawl.add(schedule);

		label1.setBounds(900, 40, 100, 30);
		crawl.add(label1);

		time1.setText(timeStamp);
		time1.setBounds(1000, 40, 100, 30);
		crawl.add(time1);

		label2.setBounds(1100, 40, 100, 30);
		crawl.add(label2);

		time2.setText(next);
		time2.setBounds(1200, 40, 100, 30);
		crawl.add(time2);

		linkLabel.setBounds(100, 300, 100, 30);
		crawl.add(linkLabel);
		crawl.add(linkLabelValues);

		descriptionLabel.setBounds(100, 330, 100, 30);
		crawl.add(descriptionLabel);
		crawl.add(descriptionLabelValues);

		keywordLabel.setBounds(100, 360, 100, 30);
		crawl.add(keywordLabel);
		crawl.add(keywordLabelValues);

		contentLabel.setBounds(100, 390, 100, 30);
		crawl.add(contentLabel);
		crawl.add(contentLabelValues);

		imageLabel.setBounds(100, 420, 100, 30);
		crawl.add(imageLabel);
		crawl.add(imageLabelValues);

		nameLabel.setBounds(100, 450, 100, 30);
		crawl.add(nameLabel);
		crawl.add(nameLabelValues);

		locationLabel.setBounds(100, 480, 100, 30);
		crawl.add(locationLabel);
		crawl.add(locationLabelValues);

		fetchedDateLabel.setBounds(100, 510, 100, 30);
		crawl.add(fetchedDateLabel);
		crawl.add(fetchedDateLabelValues);

		headerTypeLabel.setBounds(100, 540, 100, 30);
		crawl.add(headerTypeLabel);
		crawl.add(headerTypeLabelValues);

		statusLabel.setBounds(100, 570, 100, 30);
		crawl.add(statusLabel);
		crawl.add(statusLabelValues);

		// System.out.println("Done adding all");
		String title, link, mainCategory, subCategory;

		int count = 0;
		ArrayList<String> authorityLink = new ArrayList<>();
		BasicDBObject crawlDataQuery = new BasicDBObject();
		crawlDataQuery.put("systemDate", Date.getDate(0)); // parameterized
															// query. here we
															// provide the
															// condition
		DBCursor cursor = crawldataDB.find(crawlDataQuery); // If we don not
															// have query then
															// keep brackets
															// blank. It will
															// fetch all records
		while (cursor.hasNext()) {
			// System.out.println(cursor.next());
			DBObject object = cursor.next();
			count = count + 1;
			authorityLink.add(object.get("link").toString());
		}

		for (int i = 0; i < count; i++) {
			BasicDBObject crawldataQuery = new BasicDBObject();
			crawldataQuery.put("link", authorityLink.get(i)); // parameterized
																// query. here
																// we provide
																// the condition
			DBCursor crawldatacursor = crawldataDB.find(crawldataQuery); // If
																			// we
																			// don
																			// not
																			// have
																			// query
																			// then
																			// keep
																			// brackets
																			// blank.
																			// It
																			// will
																			// fetch
																			// all
																			// records
			while (crawldatacursor.hasNext()) {
				title = crawldatacursor.next().get("title").toString();
				link = authorityLink.get(i);
				/*
				 * subCategory =
				 * crawldatacursor.next().get("subCategory").toString();
				 * mainCategory =
				 * crawldatacursor.next().get("mainCategory").toString();
				 */
				subCategory = "Sub Category";
				mainCategory = "Main Category";
				// Data data1 = new Data(title, link, subCategory,
				// mainCategory);
				// data.add(data1);
			}
		}
		// System.out.println("before adding table");
		// tableData = new String[authorityLink.size()][4];

		/*
		 * for (int i = 0; i < authorityLink.size(); i++) { tableData[i][0] =
		 * data.get(i).title; tableData[i][1] = data.get(i).link;
		 * tableData[i][2] = data.get(i).mainCategory; tableData[i][3] =
		 * data.get(i).subCategory; }
		 */

		BasicDBObject settingsQuery1 = new BasicDBObject();
		settingsQuery1.put("systemName", LoginPage.gettypename()); // parameterized
		// query. here
		// we provide
		// the condition
		DBCursor settingsCursor = settingsDb.find(settingsQuery1); // If we don
																	// not have
																	// query
																	// then
		// keep brackets blank. It will
		// fetch all records
		while (settingsCursor.hasNext()) {
			// System.out.println(cursor.next());
			// System.out.println(cursor.next().get("name"));
			DBObject ognject = settingsCursor.next();
			nlp = ognject.get("nlp").toString();
			ner = ognject.get("ner").toString();
			image = ognject.get("image").toString();
			category = ognject.get("category").toString();
			translate = ognject.get("translate").toString();
			modules.clear();
			// System.out.println("Nlp " + nlp + " NER " + ner + " Image " +
			// image + " category " + category + " translate " + translate);
			modules.put("nlp", nlp);
			modules.put("ner", ner);
			modules.put("image", image);
			modules.put("category", category);
			modules.put("translation", translate);
			// In this we are storing values according to the system settings we
			// have applied.
		}

		Navigate navigate = new Navigate(crawl);
		TopPanel.dashboard.addMenuListener(navigate);
		TopPanel.settings.addMenuListener(navigate);
		TopPanel.logout.addMenuListener(navigate);
		TopPanel.training.addMenuListener(navigate);
		TopPanel.verification.addMenuListener(navigate);
		TopPanel.domainStats.addActionListener(navigate);
		TopPanel.systemStats.addActionListener(navigate);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		seedUrl = CrawlingSeeds.dbSeedUrl();
		// System.out.println("And the size is "+seedUrl.size());
		crawlingSeeds = CrawlingSeeds.crawlSeedUrl(seedUrl);

		BasicDBObject nameStructureQuery = new BasicDBObject();

		DBCursor nameStructureCursor = nameStructureDB.find(nameStructureQuery);

		while (nameStructureCursor.hasNext()) {
			DBObject object = nameStructureCursor.next();
			nameStructure.add((String) object.get("structure"));
		}

		/*
		 * float sample; sample = 100 / crawlingSeeds.size(); progressCalc =
		 * Math.round(sample); progressCalc++;
		 */
		/*
		 * Task task = new Task(); Thread thread = new Thread(task);
		 * thread.start();
		 */
		System.out.println("Fianlly for crawling we get " + crawlingSeeds.size());
		BasicDBObject sampleCrawlDataQuery = new BasicDBObject();

		for (int i = 0; i < crawlingSeeds.size(); i++) {
			Document htmlDoc = null;
			try {
				// if (!crawlingSeeds.get(j).regex.contains("no
				// regex")) {
				ArrayList<String> disAllowList = RobotChecks.robotsTXT(crawlingSeeds.get(i).url);
				// System.out.println(" Connected to " +
				// crawlingSeeds.get(j).url);

				BasicDBObject seedUrlQuery = new BasicDBObject();
				seedUrlQuery.clear();
				seedUrlQuery.put("url", crawlingSeeds.get(i).url);
				DBCursor seedUrlCursor = seedUrlDB.find(seedUrlQuery);
				String systemName = "";
				while (seedUrlCursor.hasNext()) {
					// System.out.println(cursor.next());
					systemName = seedUrlCursor.next().get("systemName").toString();
				}

				ArrayList<Crawldata> crawlList = new ArrayList<>();
				String system = systemName;
				system = system.toLowerCase();
				System.out.println("System is " + system);
				if (system.contains("rss")) {
					List<SyndEntry> entries = null;
					try {

						URL url = new URL(crawlingSeeds.get(i).url);
						System.out.println("Connected to " + crawlingSeeds.get(i).url);
						HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
						// Reading the feed
						SyndFeedInput input = new SyndFeedInput();
						SyndFeed feed = input.build(new XmlReader(httpcon));
						entries = feed.getEntries();
					} catch (Exception e1) {
						System.out.println("Problem in fetching RSS feed ");
						e1.printStackTrace();
					}
					Iterator<SyndEntry> itEntries = entries.iterator();

					while (itEntries.hasNext()) {
						SyndEntry entry = itEntries.next();

						String title = entry.getTitle();
						String authorityUrl = entry.getLink();

						Crawldata object = new Crawldata(title, authorityUrl);
						crawlList.add(object);

					}
				} else {

					String seedUrl = crawlingSeeds.get(i).url;
					htmlDoc = Jsoup.connect(seedUrl).get();
					System.out.println("Connected to " + seedUrl);
					links = htmlDoc.select("a[href]");
					for (Iterator iterator = links.iterator(); iterator.hasNext();) {
						Element link = (Element) iterator.next();
						String title = link.text();
						String authorityUrl = link.attr("abs:href");

						Crawldata object = new Crawldata(title, authorityUrl);
						crawlList.add(object);

						// Pagination links fetching
						if (title.equals("2") || title.equals("3") || title.equals("4") || title.equals("5")
								|| title.equals("6") || title.equals("7") || title.equals("8") || title.equals("9")
								|| title.equals("10")) {

							try {
								BasicDBObject seedUrlQuery1 = new BasicDBObject();
								seedUrlQuery1.clear();
								seedUrlQuery1.put("url", authorityUrl);
								DBCursor seedUrlCursor1 = seedUrlDB.find(seedUrlQuery1); // If
																							// we
																							// don
																							// not
																							// have
																							// query
																							// then
																							// keep
																							// brackets
																							// blank.
																							// It
																							// will
																							// fetch
																							// all
																							// records
								if (!seedUrlCursor1.hasNext()) {
									seedUrlQuery1.clear();
									seedUrlQuery1.put("url", authorityUrl);
									seedUrlQuery1.put("domainUrl", crawlingSeeds.get(i).domainName);
									seedUrlQuery1.put("moduleName", crawlingSeeds.get(i).moduleName);
									seedUrlQuery1.put("systemName", LoginPage.gettypename());
									seedUrlQuery1.put("addedDate", Date.getDate(0));
									seedUrlQuery1.put("subdomain/maindomain", crawlingSeeds.get(i).subMain);
									seedUrlQuery1.put("bucketNumber", "0");
									seedUrlQuery1.put("nextCrawlDate", Date.getDate(0));
									seedUrlQuery1.put("type", crawlingSeeds.get(i).type);
									seedUrlDB.insert(seedUrlQuery1);
									seedUrlQuery1.clear();
								}
							} catch (Exception e1) // If record
													// not
													// present
													// then
													// spring
													// framework
													// throws
													// exception
							{
								e1.printStackTrace();
							}
						}
					}
				}
				for (int k = 0; k < crawlList.size(); k++) {
					try {
						String authorityUrl = crawlList.get(k).url;
						System.out.println("Trying to connect " + authorityUrl);
						Document doc = Jsoup.connect(authorityUrl).get();
						System.out.println("Connected to " + authorityUrl);
						String title = crawlList.get(k).title;
						sampleCrawlDataQuery.clear();
						sampleCrawlDataQuery.put("link", authorityUrl);
						DBCursor sampleCrawlDataCursor = sampleCrawlDataDB.find(sampleCrawlDataQuery);
						if (!sampleCrawlDataCursor.hasNext()) {

							title = RemoveSpecialChar.removeSpecialChars(title);
							title = title.trim();

							if (!title.isEmpty()) {
								boolean filters = true;
								// if
								// (authorityUrl.contains(crawlingSeeds.get(i).regex)
								// &&
								// !authorityUrl.contains(crawlingSeeds.get(i).noRegex))
								// { // 3
								// level
								// filter
								// lines
								// will
								// come
								// here
								if (crawlingSeeds.get(i).regex.contains("(^.+?\\d$)")) {
									if (authorityUrl.matches("(^.+?\\d$)")
											&& !authorityUrl.contains(crawlingSeeds.get(i).noRegex)) {
										filters = true;
									} else
										filters = false;
								} else if (crawlingSeeds.get(i).regex.contains("(.*(")) {
									System.out.println("Initial value is " + filters);
									System.out.println(authorityUrl);
									if (authorityUrl.matches(crawlingSeeds.get(i).regex)
											&& !authorityUrl.contains(crawlingSeeds.get(i).noRegex)) {
										filters = true;
										System.out.println("Filters is true");
									} else {
										filters = false;
										System.out.println("Filters is false");
									}
								} else {
									if (authorityUrl.contains(crawlingSeeds.get(i).regex)
											&& !authorityUrl.contains(crawlingSeeds.get(i).noRegex)) {
										filters = true;
									} else
										filters = false;
								}
								if (filters == true) {
									System.out.println("Link is clear");
									boolean nameFlag = true;
									for (int g = 0; g < nameStructure.size(); g++) {
										if (title.contentEquals(nameStructure.get(g))) {
											nameFlag = false;
											break;
										} else {
											nameFlag = true;
										}
									}
									if (nameFlag == true) {
										String fileName = "";
										boolean robotsFlag = true;
										for (int j = 0; j < disAllowList.size(); j++) {
											if (authorityUrl.contains(disAllowList.get(j))) {
												robotsFlag = false;
												break;
											} else {
												robotsFlag = true;
											}
										}
										if (robotsFlag == true) {

											// index noindex check
											if (RobotChecks.metaRobots(doc).contentEquals("index")) {
												System.out.println("Page is indexable");
												String description, keyword;
												String nospaceTitleDescription;
												description = MetaData.metaData(doc, crawlingSeeds.get(i).description);
												description = RemoveSpecialChar.removeSpecialChars(description);
												description = description.trim();
												nospaceTitleDescription = title;
												nospaceTitleDescription = nospaceTitleDescription + description;
												nospaceTitleDescription = nospaceTitleDescription.replaceAll("\\s+",
														"");
												System.out.println(nospaceTitleDescription);
												if (!description.isEmpty()) {
													System.out.println("Description is not empty");
													sampleCrawlDataQuery.clear();
													BasicDBObject sampleCrawlDataQueryAND = new BasicDBObject();
													sampleCrawlDataQueryAND.put("nospaceTitleDescription",
															nospaceTitleDescription);

													// System.out.println(sampleCrawlDataQueryAND.toString());

													DBCursor sampleCrawlDataCursorAND = sampleCrawlDataDB
															.find(sampleCrawlDataQueryAND);
													System.out.println("Checking for no space title description "
															+ nospaceTitleDescription);
													if (!sampleCrawlDataCursorAND.hasNext()) {
														System.out.println("Title, description are unique");
														keyword = MetaData.metaData(doc, crawlingSeeds.get(i).keyword);
														keyword = RemoveSpecialChar.removeSpecialChars(keyword);
														keyword = keyword.trim();

														String translatedTitle, translatedDescription,
																translatedKeyword;
														if (CrawlerPage.modules.get("translation").contains("no")) {
															translatedTitle = "";
															translatedDescription = "";
															translatedKeyword = "";
														} else {
															if (!Translation.languageDetectIsEnglish(title)) {
																translatedTitle = Translation.languageTranslate(title);
															} else {
																translatedTitle = "";
															}
															if (!Translation.languageDetectIsEnglish(description)) {
																translatedDescription = Translation
																		.languageTranslate(description);
															} else {
																translatedDescription = "";
															}
															if (!Translation.languageDetectIsEnglish(keyword)) {
																translatedKeyword = Translation
																		.languageTranslate(keyword);
															} else {
																translatedKeyword = "";
															}
														}
														String systemDate = Date.getDate(0);

														// ner code goes
														// here
														String location = "-", personName = "-", leadDate = "-",
																companyName = "-";
														String mainContent = "";
														if (CrawlerPage.modules.get("ner").contains("yes")) {

															// Boilerpipe
															// code
															// will be here
															// NER code will
															// be
															// here and the
															// values will
															// be
															// stored in
															// above
															// mentioned
															// variables.
															URL url = null;
															try {
																System.out.println("Ner is for " + authorityUrl);
																url = new URL(authorityUrl);
															} catch (MalformedURLException e1) {
																// TODO
																// Auto-generated
																// catch
																// block
																e1.printStackTrace();
															}

															// main content
															// of a
															// url is
															// fetched
															// here using
															// boilerpipe
															// api
															BoilerPipeMainText bpmt = new BoilerPipeMainText(url);
															mainContent = bpmt.process();

															// location
															// person
															// name,
															// organization
															// name
															// and date is
															// fetched from
															// main
															// content using
															// Stanford NER
															StanFordNER stanford = new StanFordNER(mainContent);
															HashMap hm = stanford.getDataForAllClasses();
															leadDate = hm.get("date").toString();
															location = hm.get("location").toString();
															personName = hm.get("person").toString();
															companyName = hm.get("organization").toString();

															if (personName.length() > 2)
																personName = personName.substring(1,
																		personName.length() - 1);
															else
																personName = "-";

															if (location.length() > 2)
																location = location.substring(1, location.length() - 1);
															else
																location = "-";

															if (leadDate.length() > 2)
																leadDate = leadDate.substring(1, leadDate.length() - 1);
															else
																leadDate = "-";

															if (companyName.length() > 2)
																companyName = companyName.substring(1,
																		companyName.length() - 1);
															else
																companyName = "-";

															/*
															 * System.out.
															 * println(
															 * "Lead Date "
															 * +leadDate);
															 * System.out.
															 * println(
															 * "Person Name "
															 * +personName);
															 * System.out.
															 * println(
															 * "Location "
															 * +location);
															 * System.out.
															 * println(
															 * "Company Name "
															 * +companyName) ;
															 */
														}

														String imageSrc = "", fevicon = "";
														if (CrawlerPage.modules.get("image").contains("yes")) {

															// Fetches the
															// main image
															// from the link
															// provided and
															// is stored in
															// s3 bucket and
															// localdirectory

															System.out.println("Image is for " + authorityUrl);
															GetByGoose getGoose = new GetByGoose(authorityUrl);
															imageSrc = getGoose.ImageValue();

															if (imageSrc != "no.jpg") {
																try {
																	new S3upload(imageSrc);
																} catch (IOException e2) {

																	e2.printStackTrace();
																}

															}

															// fetched
															// fevicon from
															// provided lnk
															// and is stored
															// on s3 bucket.
															fevicon = DownloadFeviconImage
																	.downloadFaviconImageAndGetPath(doc,
																			crawlingSeeds.get(i).domainName);
															if (!fevicon.equalsIgnoreCase("not found")) {
																fevicon = fevicon.substring(
																		fevicon.lastIndexOf("\\") + 1,
																		fevicon.length());
																try {
																	new S3upload(fevicon);
																} catch (IOException e1) {

																	e1.printStackTrace();
																}
															}
														} else
															imageSrc = "no.jpg";

														// Latitude and
														// Longitude
														// will
														// be fetched here
														String geoLocation = GeoLocation.updateLocation(location);

														// Category code goes
														// here
														String subCategory = "";
														// if
														// (CrawlerPage.modules.get("category").contains("yes"))
														// {
														// subCategory =
														// CategoryClassification.GetType(mainContent);
														// //Weka code comes
														// here.
														// When perfect do
														// unlock
														// this
														subCategory = crawlingSeeds.get(i).type;
														// }
														// insert into following
														// table
														// if
														// (!description.isEmpty())
														// {
														/*
														 * insertCrawlData(
														 * crawlingSeeds
														 * .get(i).domainName,
														 * authorityUrl, title,
														 * description, keyword,
														 * leadDate,
														 * mainCategory,
														 * subCategory, "", 0,
														 * "", "",
														 * translatedTitle,
														 * translatedDescription,
														 * translatedKeyword,
														 * location, "",
														 * personName,
														 * systemDate, imageSrc,
														 * fevicon, companyName,
														 * "", "", "", "", "",
														 * "", "", "", "", "",
														 * "", "", "", "", "",
														 * "", "", "", "", "",
														 * "", "", "", "", "",
														 * "", "", "", "", "",
														 * "", "", "", "", "",
														 * "", "", "", "", "",
														 * "", "", "", "", "",
														 * "",
														 * crawlingSeeds.get(i).
														 * moduleName,
														 * mainContent,
														 * imageSrc,
														 * geoLocation);
														 */
														int followersCount = 0;
														String expiredDate = "", expiredStatus = "";
														String industry = "";
														insertCrawlData(crawlingSeeds.get(i).domainName, authorityUrl,
																title, description, subCategory,
																crawlingSeeds.get(i).moduleName, systemDate, location,
																translatedTitle, translatedDescription,
																translatedKeyword, industry, personName, companyName,
																followersCount, leadDate, geoLocation, mainContent,
																imageSrc, expiredDate, expiredStatus, keyword, imageSrc,
																fevicon, nospaceTitleDescription);

														Data dataObject = new Data(title, authorityUrl, industry,
																subCategory, description, keyword, mainContent,
																imageSrc, personName, location, leadDate,
																statusHeader.getHeader(doc),
																statusHeader.getStatus(authorityUrl), imageSrc);

														System.out.println("Data Object is " + dataObject);
														data.add(dataObject);
														// }
													}
												}
											} else {
												/*
												 * System.out.println(
												 * "&&&&&&&&&&&&&&&&&&&&&&&" );
												 * System.out.println(
												 * "No index for " +
												 * authorityUrl);
												 * System.out.println(
												 * "##############################"
												 * ); System.out.println(
												 * "Size of DATA is "
												 * +data.size());
												 */
											}
										} else {
											// System.out.println("Robots.txt
											// blocked " + authorityUrl);
										}
									}
								}
							}
						}
					} catch (Exception e1) {
						System.out.println("Not connected to authority url");
					}
				}
				crawlList.clear();
				// }
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			try {
				updatenextCrawlDate(crawlingSeeds.get(i).url, crawlingSeeds.get(i).bucketNumber,
						crawlingSeeds.get(i).nextCrawlDate);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		// table code ithe lihi
		String[][] tableData = new String[data.size()][4];
		System.out.println("Size of DATA is " + data.size());
		for (int k = 0; k < data.size(); k++) {
			tableData[k][0] = data.get(k).title;
			tableData[k][1] = data.get(k).link;
			tableData[k][2] = data.get(k).mainCategory;
			tableData[k][3] = data.get(k).subCategory;
			System.out.println("*****************************************************************");
			System.out.println(data.get(k).title + " " + data.get(k).link);
		}
		// System.out.println("Model is here");

		// System.out.println("Done adding table");

		String[] header = { "Title", "Link", "Main Category", "Sub Category" }; // Table
		// header
		// values
		final JTable table = new JTable(tableData, header);
		JScrollPane scrollbar = new JScrollPane(table);
		/*
		 * TableModel model = new DefaultTableModel(tableData, header) { public
		 * boolean isCellEditable(int row, int column) { return false;// This
		 * causes all cells to be not editable } };
		 */
		// table = new JTable(model);

		crawl.add(table);
		scrollbar = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		scrollbar.setBounds(0, 100, 1370, 200);
		crawl.add(scrollbar);

		table.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent event) {
				int row = table.rowAtPoint(event.getPoint());
				int col = table.columnAtPoint(event.getPoint());

				if (row >= 0 && col == 0) {
					linkLabelValues.setText(table.getValueAt(row, col + 1).toString());
					linkLabelValues.setBounds(300, 300, 600, 30);

					descriptionLabelValues.setText(data.get(row).description);
					descriptionLabelValues.setBounds(300, 330, 600, 30);
					keywordLabelValues.setText(data.get(row).keyword);
					keywordLabelValues.setBounds(300, 360, 600, 30);
					contentLabelValues.setText(data.get(row).mainContent);
					contentLabelValues.setBounds(300, 390, 600, 30);

					imageLabelValues.setText(data.get(row).image);
					imageLabelValues.setBounds(300, 420, 600, 30);

					nameLabelValues.setText(data.get(row).personName);
					nameLabelValues.setBounds(300, 450, 600, 30);

					locationLabelValues.setText(data.get(row).location);
					locationLabelValues.setBounds(300, 480, 600, 30);

					fetchedDateLabelValues.setText(data.get(row).fetchDate);
					fetchedDateLabelValues.setBounds(300, 510, 600, 30);

					headerTypeLabelValues.setText(data.get(row).headerType);
					headerTypeLabelValues.setBounds(300, 540, 600, 30);

					statusLabelValues.setText(data.get(row).status);
					statusLabelValues.setBounds(300, 570, 600, 30);

					JPanel imagePanel = new JPanel();
					imagePanel.setBounds(1000, 350, 300, 300);
					imagePanel.setLayout(null);
					BufferedImage image = null;
					try {
						image = ImageIO.read(new File("./IMAGES/" + data.get(row).image));
					} catch (IOException e) {
						e.printStackTrace();
					}
					Image scaledImage = image.getScaledInstance(imagePanel.getWidth(), imagePanel.getHeight(),
							Image.SCALE_SMOOTH);
					ImageIcon background = new ImageIcon(scaledImage);
					JLabel label = new JLabel();
					label.setBounds(0, 0, 300, 300);
					label.setIcon(background);

					imagePanel.add(label);
					crawl.add(imagePanel);

				}
				crawl.repaint();

			}
		});
	}

	/*
	 * public void insertCrawlData(String domainName, String authorityUrl,
	 * String title, String description, String keyword, String leadDate, String
	 * mainCategory, String subCategory, String type, int is_star, String
	 * expiredDate, String expiredStatus, String translatedTitle, String
	 * translatedDescription, String translatedKeywords, String location, String
	 * regions, String personName, String systemDate, String image, String
	 * fevicon, String companyName, String specialist, String industry, String
	 * companySize, String founded, String alexaRanking, String webServer,
	 * String hostingProvider, String framework, String SSlCertificate, String
	 * language, String nameServer, String investorName, String status, String
	 * investments, String fundRaised, String alaises, String comapanyEmail,
	 * String companyNumber, String rating, String starring, String version,
	 * String size, String compatibility, String amount, String pages, String
	 * ISBN, String OCLC, String preceddedBy, String followedBy, String
	 * followersCount, String twitter_link, String linkedin_link, String
	 * facebook_link, String gmail_link, String twitter_image, String
	 * linkedin_image, String facebook_image, String gmail_image, String
	 * twitter_location, String linkedin_location, String facebook_location,
	 * String gmail_location, String developers, String ownership, String
	 * LCClass, String mediatype, String moduleName, String mainContent, String
	 * imagesrc, String geoLocation) { BasicDBObject insertObject = new
	 * BasicDBObject(); insertObject.put("domainLink", domainName);
	 * insertObject.put("link", authorityUrl); insertObject.put("title", title);
	 * insertObject.put("description", description); insertObject.put("keyword",
	 * keyword); insertObject.put("leadDate", leadDate);
	 * insertObject.put("mainCategory", type); insertObject.put("subCategory",
	 * subCategory); insertObject.put("type", type); insertObject.put("is_star",
	 * 0); insertObject.put("expiredDate", expiredDate);
	 * insertObject.put("expiredStatus", expiredStatus);
	 * insertObject.put("translatedTitle", translatedTitle);
	 * insertObject.put("translatedDescription", translatedDescription);
	 * insertObject.put("translatedKeyword", translatedKeywords);
	 * insertObject.put("location", location); insertObject.put("regions",
	 * regions); insertObject.put("personName", personName);
	 * insertObject.put("systemDate", systemDate); insertObject.put("image",
	 * image); insertObject.put("fevicon", fevicon);
	 * insertObject.put("companyName", companyName);
	 * insertObject.put("specialist", specialist); insertObject.put("industry",
	 * industry); insertObject.put("companySize", companySize);
	 * insertObject.put("founded", founded); insertObject.put("alexaRanking",
	 * alexaRanking); insertObject.put("webServer", webServer);
	 * insertObject.put("hostingProvider", hostingProvider);
	 * insertObject.put("framework", framework);
	 * insertObject.put("SSLCertificate", SSlCertificate);
	 * insertObject.put("language", language); insertObject.put("nameServer",
	 * nameServer); insertObject.put("investorName", investorName);
	 * insertObject.put("status", status); insertObject.put("investments",
	 * investments); insertObject.put("fundRaised", fundRaised);
	 * insertObject.put("alaises", alaises); insertObject.put("companyEmail",
	 * comapanyEmail); insertObject.put("companyNumber", companyNumber);
	 * insertObject.put("rating", rating); insertObject.put("starring",
	 * starring); insertObject.put("version", version); insertObject.put("size",
	 * size); insertObject.put("compatibility", compatibility);
	 * insertObject.put("amount", amount); insertObject.put("pages", pages);
	 * insertObject.put("ISBN", ISBN); insertObject.put("OCLC", OCLC);
	 * insertObject.put("precededBy", preceddedBy);
	 * insertObject.put("followedBy", followedBy);
	 * insertObject.put("followersCount", followersCount);
	 * insertObject.put("twitter_link", twitter_link);
	 * insertObject.put("linkedin_link", linkedin_link);
	 * insertObject.put("facebook_link", facebook_link);
	 * insertObject.put("gmail_link", gmail_link);
	 * insertObject.put("twitter_image", twitter_image);
	 * insertObject.put("linkedin_image", linkedin_image);
	 * insertObject.put("facebook_image", facebook_image);
	 * insertObject.put("gmail_image", gmail_image);
	 * insertObject.put("twitter_location", twitter_location);
	 * insertObject.put("linkedin_location", linkedin_location);
	 * insertObject.put("facebook_location", facebook_location);
	 * insertObject.put("gmail_location", gmail_location);
	 * insertObject.put("developers", developers); insertObject.put("ownership",
	 * ownership); insertObject.put("LCClass", LCClass);
	 * insertObject.put("mediaType", mediatype); insertObject.put("moduleName",
	 * moduleName); insertObject.put("mainContent", mainContent);
	 * insertObject.put("localImagepath", imagesrc);
	 * insertObject.put("geoLocation", geoLocation);
	 * sampleCrawlDataDB.insert(insertObject); crawldataDB.insert(insertObject);
	 * insertObject.clear(); // It // needs // to // get // clear // to //
	 * update // next // records; }
	 */
	public void insertCrawlData(String domainLink, String link, String title, String description, String subCategory,
			String moduleName, String systemDate, String location, String translatedTitle, String translatedDescription,
			String translatedKeyword, String industry, String personName, String companyName, int followersCount,
			String leadDate, String geoLocation, String mainContent, String imageLocalPath, String expiredDate,
			String expiredStatus, String keyword, String imageSrc, String fevicon, String nospaceTitleDescription) {
		BasicDBObject insertObject = new BasicDBObject();

		insertObject.put("domainLink", domainLink);
		insertObject.put("link", link);
		insertObject.put("title", title);
		insertObject.put("description", description);
		insertObject.put("subCategory", subCategory);
		insertObject.put("moduleName", moduleName);
		insertObject.put("systemDate", systemDate);
		insertObject.put("location", location);
		insertObject.put("translatedTitle", translatedTitle);
		insertObject.put("translatedKeyword", translatedKeyword);
		insertObject.put("translatedDescription", translatedDescription);
		insertObject.put("industry", industry);
		insertObject.put("personName", personName);
		insertObject.put("companyName", companyName);
		insertObject.put("followersCount", followersCount);
		insertObject.put("leadDate", leadDate);
		insertObject.put("geoLocation", geoLocation);
		insertObject.put("mainContent", mainContent);
		insertObject.put("imageLocalPath", imageLocalPath);
		insertObject.put("expiredDate", expiredDate);
		insertObject.put("expiredStatus", expiredStatus);
		insertObject.put("keyword", keyword);
		insertObject.put("image", imageSrc);
		insertObject.put("fevicon", fevicon);
		insertObject.put("nospaceTitleDescription", nospaceTitleDescription);
		sampleCrawlDataDB.insert(insertObject);
		crawldataDB.insert(insertObject);
		insertObject.clear();
	}

	public void updatenextCrawlDate(String url, int bucketNumber, String prevCrawlDate) throws ParseException {

		bucketNumber = bucketNumber + 1;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(prevCrawlDate));
		c.add(Calendar.DATE, bucketNumber); // number of days to add
		String nextCrawlDate = sdf.format(c.getTime());

		BasicDBObject seedUrlQuery = new BasicDBObject();
		seedUrlQuery.put("url", url);
		DBCursor testItemsCursor = seedUrlDB.find(seedUrlQuery);
		while (testItemsCursor.hasNext()) {
			DBObject updateItem = testItemsCursor.next();
			updateItem.put("nextCrawlDate", nextCrawlDate);
			seedUrlDB.save(updateItem);

		}
	}
}

class Data {
	String title, link, mainCategory, subCategory, description, keyword, mainContent, image, personName, location,
			fetchDate, headerType, status, imagesrc;

	public Data(String title, String link, String mainCategory, String subCategory, String description, String keyword,
			String mainContent, String image, String personName, String location, String fetchDate, String headerType,
			String status, String imagesrc) {
		super();
		this.title = title;
		this.link = link;
		this.mainCategory = mainCategory;
		this.subCategory = subCategory;
		this.description = description;
		this.keyword = keyword;
		this.mainContent = mainContent;
		this.image = image;
		this.personName = personName;
		this.location = location;
		this.fetchDate = fetchDate;
		this.headerType = headerType;
		this.status = status;
		this.imagesrc = imagesrc;
	}

}

class Crawldata {
	String title, url;

	public Crawldata(String title, String url) {
		this.title = title;
		this.url = url;
	}
}
