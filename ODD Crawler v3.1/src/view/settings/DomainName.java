package view.settings;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Ganesh
 *
 *         This class is used for getting proper DomainName on URL
 */
public class DomainName {

	/**
	 * this method will accept main domain URL as parameter and will give main
	 * domain name and return it as String
	 * 
	 * @param mainDomainUrl
	 *            Here we will send main Domain URL
	 * @return String It will return Domain name according to got Domain URL
	 *         from mainDomainUrl parameter
	 */

	public static String getMainDomainName(String mainDomainUrl) {
		try {
			URL url1 = new URL(mainDomainUrl);
			String host = url1.getHost();
			// String netHost=host.substring(host.indexOf(".")+1);
			return host;
		} catch (Exception e) {
			return "Invalid Url in DomainName";
		}
	}

	/**
	 * this method will accept sub domain URL as parameter and will give main
	 * domain name and return it as String
	 * 
	 * @param subDomainUrl
	 *            This parameter will take sub Domain URL
	 * @return String It will return domain name according to got URL from
	 *         subDomainUrl parameter
	 * @throws URISyntaxException
	 *             URISyntax Exception will throw whenever you pass the wrong or
	 *             informed URL in the subDomainUrl parameter
	 */
	public static String getSubDomainName(String subDomainUrl) throws URISyntaxException {
		String w = "www.";
		Pattern p = Pattern.compile(".*?([^.]+\\.[^.]+)");
		URI uri = new URI(subDomainUrl);
		// eg: uri.getHost() will return "www.foo.com"
		Matcher m = p.matcher(uri.getHost());
		if (m.matches()) {
		}
		return w + m.group(1);
	}

	/**
	 * this method will check what is the type of URL and according to type it
	 * will call that particular method and will return Domain Name as String
	 * 
	 * @param urlType
	 *            This Parameter will accept domain type as string for example
	 *            Sub Domain or Main Domain and according to type we will call
	 *            getMainDommainName(url) or getSubDommainName(url) method
	 * @param url
	 *            this parameter will accept URl
	 * @return String here we return domain name as String
	 * @throws URISyntaxException
	 *             URISyntax Exception will throw whenever you pass the wrong or
	 *             informed URL in the subDomainUrl parameter
	 */
	public static String getDomainName(String urlType, String url) throws URISyntaxException {

		if (urlType.equals("maindomain")) {
			return getMainDomainName(url);
		} else {
			return getSubDomainName(url);
		}
	}

	/*
	 * public static void main(String[] args) throws URISyntaxException {
	 * 
	 * 
	 * System.out.println(getSubDomainName("http://www.devnet.Kentico.com"));
	 * System.out.println(getSubDomainName("http://www.bremen.craigslist.org"));
	 * System.out.println(getSubDomainName("http://www.tanger.locanto.ma"));
	 * System.out.println(getSubDomainName("http://western.locanto.com"));
	 * 
	 * System.out.println("");
	 * 
	 * System.out.println(getMainDomainName(
	 * "https://www.dice.com/jobs?q=telecommute&l="));
	 * System.out.println(getMainDomainName("http://www.simplyhired.com"));
	 * System.out.println(getMainDomainName("http://www.classifieds.co.zw"));
	 * System.out.println(getMainDomainName("http://www.olx.in"));
	 * 
	 * 
	 * 
	 * }
	 */
}
