/**
 * In this package we can find Setting Page and Add new Crawler page classes 
 */
package view.settings;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import view.settings.EHSettingPage;
import view.topPanel.Navigate;
import view.topPanel.TopPanel;

/**
 * @author Ganesh This is Main Class for Setting Page which is extends JFrame.
 *         Inside this we can Find Some component which is added on it
 *
 */
public class SettingPage extends JFrame {

	Dimension screenSize = null;
	/**
	 * borderLayoutCenter, It is panel which is added to SettingPage and on it
	 * we added table panel
	 */
	JPanel borderLayoutCenter = null;

	public SettingPageRowHolderJPanel table = null;
	JScrollPane scrollPane = null;

	public JButton btn_AddNewCrawler = null;
	/**
	 * This is Event Delegation model. In EHSettingPage class we can find event
	 * handling method and code for Setting Page
	 */
	EHSettingPage handler = null;

	public SettingPage() {
		// TODO Auto-generated constructor stub
		super("Settings");
		screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		borderLayoutCenter = new JPanel();
		table = new SettingPageRowHolderJPanel(); // it is table pan on table
													// pan we added some
													// component if you want
													// which this added on it
													// then please check
													// SettingPageRowHolderJpanel
													// class
		scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		btn_AddNewCrawler = new JButton("Add New Crawler");
		handler = new EHSettingPage(this);// here we pass the current object to
											// EHSettingPage class for holding
											// Event Source of current object

		btn_AddNewCrawler.setBounds(1000, 420, 150, 25);
		scrollPane.setBounds(70, 150, 1200, 260);

		// here we added All component to borderLayoutCenter Panel
		borderLayoutCenter.setLayout(null);
		borderLayoutCenter.add(scrollPane);
		borderLayoutCenter.add(btn_AddNewCrawler);
		borderLayoutCenter.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		btn_AddNewCrawler.addActionListener(handler);

		// adding action listener for each row
		for (Map.Entry<String, SettingPageRowJPanel> entry : table.rowMap.entrySet()) {

			entry.getValue().btn_Edit.addActionListener(handler);
			entry.getValue().btn_Delete.addActionListener(handler);
			// System.out.println(entry.getKey()+" :
			// "+entry.getValue().lbl_CrawlerName.getText());
		}

		this.setVisible(true);
		this.setJMenuBar(TopPanel.getPanel());// here we added menu bar for more
												// information please check
												// TopPanel class and its
												// getPanel method
		this.add(borderLayoutCenter, BorderLayout.CENTER);
		// this.setSize(screenSize.width, screenSize.height);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		// Here we use Event delegation model for menu bar. We handle Events Of
		// menu bar in Navigate class for more information please check
		Navigate navigate = new Navigate(this);
		TopPanel.dashboard.addMenuListener(navigate);
		TopPanel.settings.addMenuListener(navigate);
		TopPanel.logout.addMenuListener(navigate);
		TopPanel.training.addMenuListener(navigate);
		TopPanel.verification.addMenuListener(navigate);
		TopPanel.domainStats.addActionListener(navigate);
		TopPanel.systemStats.addActionListener(navigate);

	}

}
