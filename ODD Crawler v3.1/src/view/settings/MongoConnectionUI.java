package view.settings;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.bson.Document;

import com.mongodb.MongoWriteException;
import com.mongodb.client.ListIndexesIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;

/**
 * @author Ganesh this class is used for creating mongo UI, Connection and
 *         saving data to database
 *
 */
public class MongoConnectionUI extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JButton UploadData = null;

	/**
	 * Creating Mongo UI and add component
	 */
	public MongoConnectionUI() {

		UploadData = new JButton("UploadData");

		UploadData.addActionListener(this);
		UploadData.setBounds(200, 200, 100, 60);
		this.setLayout(null);
		this.setSize(500, 500);
		this.setVisible(true);
		this.add(UploadData);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub

		// It will get todays date
		String sysDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString();

		// this is Upload Data button event
		if (event.getSource() == this.UploadData) {
			File selectedFile = null;
			SimpleExcelReaderExample reader = null;
			List<SeedUrl> seedUrlData = null;
			JOptionPane.showMessageDialog(this,
					"Please Add Excel File which is in proper format.\nDo not add in url column Dublicate records in Excel File,\nIf there is any dublicate records,\nthen please remove it using Excel tool \"remove Duplicates\"");
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel Files", "xlsx");
			fileChooser.setFileFilter(filter);
			int result = fileChooser.showOpenDialog(this);
			if (result == JFileChooser.APPROVE_OPTION) {
				selectedFile = fileChooser.getSelectedFile();
				System.out.println("Selected file: " + selectedFile.getAbsolutePath());
				reader = new SimpleExcelReaderExample();
				try {
					seedUrlData = reader.readBooksFromExcelFile(selectedFile.getAbsolutePath());
					MongoDatabase db = ConnectMongoDB.connectToMongoDB();

					/*
					 * ListIndexesIterable<Document>
					 * ind=db.getCollection("SeedUrl").listIndexes();
					 * MongoCursor<Document> cur=ind.iterator();
					 * while(cur.hasNext()) { System.out.println(cur.next().); }
					 */

					// it will create unique indexes
					db.getCollection("SeedUrl").createIndex(new Document("url", 1), new IndexOptions().unique(true));
					db.getCollection("domain").createIndex(new Document("domainName", 1),
							new IndexOptions().unique(true));

					Iterator<SeedUrl> data = seedUrlData.iterator();

					while (data.hasNext()) {
						SeedUrl temp = data.next();

						System.out.println(
								"Domain Type -  " + temp.getSubDomainOrMaindomain() + "  Url -  " + temp.getUrl());
						// it will insert data into database
						try {
							db.getCollection("SeedUrl")
									.insertOne(new Document("url", temp.getUrl())
											.append("domainUrl",
													DomainName.getDomainName(temp.getSubDomainOrMaindomain(),
															temp.getUrl()))
											.append("moduleName", temp.getModuleName())
											.append("systemName", temp.getSystemName()).append("addedDate", sysDate).// getFrom
																														// excel
																														// file
											append("subdomain/maindomain", temp.getSubDomainOrMaindomain())
											.append("bucketNumber", 0).append("nextCrawlDate", "2016-03-19"));

							db.getCollection("domain")
									.insertOne(
											new Document("domainName",
													DomainName.getDomainName(temp.getSubDomainOrMaindomain(),
															temp.getUrl())).append("keyword", temp.getKeyWords())
																	.append("description", temp.getDescription())
																	.append("regex", temp.getRegx())
																	.append("systemName", temp.getSystemName())
																	.append("addedDate", sysDate));
						} catch (MongoWriteException e) {
							data.next();
							System.out.println("Find duplicate value");
							// e.printStackTrace();
						}

					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (OpenXML4JException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				JOptionPane.showMessageDialog(this,
						"You did not selecte file please select Excel file for upload data");

			}

			System.out.println("Upload Data Succesfullly");

		}

	}

}
