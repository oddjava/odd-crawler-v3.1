package view.settings;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

import view.settings.EHAddCrawlerPage;
import view.topPanel.Navigate;
import view.topPanel.TopPanel;

/**
 * This is the AddCrawler Page class
 * 
 * @author Ganesh
 *
 */
public class AddCrawlerPage extends JFrame {

	Dimension screenSize = null;
	public JPanel borderLayoutCenter = null;
	public AddCrawlerPageTableJPanel tablepan = null;
	public Boolean newOldFlag = false;

	EHAddCrawlerPage eventHandler = null;

	/**
	 * this constructor will added component on frame and will do some setting
	 */
	public AddCrawlerPage() {
		super("Add Crwaler Page");

		borderLayoutCenter = new JPanel();
		tablepan = new AddCrawlerPageTableJPanel();
		eventHandler = new EHAddCrawlerPage(this);

		tablepan.btn_UploadFile_SeedURL.addActionListener(eventHandler);
		tablepan.btn_UploadFile_NLP.addActionListener(eventHandler);
		tablepan.btn_UploadFile_Categration.addActionListener(eventHandler);

		tablepan.chkbox_SeddURL.addActionListener(eventHandler);
		tablepan.chkbox_NLP.addActionListener(eventHandler);
		tablepan.chkbox_Categaration.addActionListener(eventHandler);

		tablepan.btn_Apply.addActionListener(eventHandler);
		tablepan.btn_Back.addActionListener(eventHandler);

		tablepan.txt_CrwalerName.addKeyListener(eventHandler);

		screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		// Head=new JLabel("Add Crawler Page");
		// Head.setFont(new Font("the times roman", Font.BOLD, 30));
		// Head.setBounds(500, 10, 300, 100);

		// tablepan.setBounds(400, 180, 500, 290);

		borderLayoutCenter.setLayout(null);
		// borderLayoutCenter.setBounds(400,120,500,400);
		borderLayoutCenter.add(tablepan);

		this.setJMenuBar(TopPanel.getPanel());

		// this.pack();
		// this.setSize(screenSize.width, screenSize.height);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);

		this.setVisible(true);

		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		this.add(borderLayoutCenter, BorderLayout.CENTER);
		// this is event delegation model we handle menu event in Navigate class
		// for more information please check Navigate class
		Navigate navigate = new Navigate(this);
		TopPanel.dashboard.addMenuListener(navigate);
		TopPanel.settings.addMenuListener(navigate);
		TopPanel.logout.addMenuListener(navigate);
		TopPanel.training.addMenuListener(navigate);
		TopPanel.verification.addMenuListener(navigate);
		TopPanel.domainStats.addActionListener(navigate);
		TopPanel.systemStats.addActionListener(navigate);

	}

}
