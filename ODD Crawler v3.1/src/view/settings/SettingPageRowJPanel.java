package view.settings;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.text.DateFormatter;

/**
 * @author Ganesh This Class is used for storing documents(row) which is fetched
 *         from Mongo Database
 */
public class SettingPageRowJPanel extends JPanel {

	public JLabel lbl_CrawlerName = null;
	public JCheckBox chk_NLP = null;
	public JCheckBox chk_NER = null;
	public JCheckBox chk_Image = null;
	public JCheckBox chk_Category = null;
	public JCheckBox chk_translation = null;

	SpinnerDateModel timemodel = null;
	public JSpinner spinner = new JSpinner();
	JSpinner.DateEditor editor = null;
	DateFormatter formatter = null;

	// public JLabel lbl_Time=null;
	public JLabel lbl_Days = null;
	public JButton btn_Edit = null;
	public JButton btn_Delete = null;

	/**
	 * here we will add all component on panel
	 */
	public SettingPageRowJPanel() {
		lbl_CrawlerName = new JLabel("CrawlerName");
		chk_NLP = new JCheckBox();
		chk_NER = new JCheckBox();
		chk_Image = new JCheckBox();
		chk_Category = new JCheckBox();
		chk_translation = new JCheckBox();

		// lbl_Time=new JLabel("HH:MM:SS");
		lbl_Days = new JLabel("Days");
		btn_Edit = new JButton("Edit");
		btn_Delete = new JButton("Delete");

		chk_NLP.setEnabled(false);
		chk_NER.setEnabled(false);
		chk_Image.setEnabled(false);
		chk_Category.setEnabled(false);
		chk_translation.setEnabled(false);

		lbl_CrawlerName.setBounds(25, 15, 100, 15);
		chk_NLP.setBounds(180, 12, 20, 20);
		chk_NER.setBounds(310, 12, 20, 20);
		chk_Image.setBounds(440, 12, 20, 20);
		chk_Category.setBounds(570, 12, 20, 20);
		chk_translation.setBounds(700, 12, 20, 20);

		timemodel = new SpinnerDateModel();

		// spinner.setBounds(815, 15, 100, 15);
		// lbl_Time.setBounds(815, 15, 100, 15);
		lbl_Days.setBounds(920, 15, 100, 15);
		btn_Edit.setBounds(1000, 10, 70, 25);
		btn_Delete.setBounds(1080, 10, 70, 25);

		this.setLayout(null);
		// this.setBounds(10,10,1200,45);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.add(lbl_CrawlerName);
		this.add(chk_NLP);
		this.add(chk_NER);
		this.add(chk_Image);
		this.add(chk_Category);
		this.add(chk_translation);

		// this.add(spinner);
		// this.add(lbl_Time);
		this.add(lbl_Days);
		this.add(btn_Edit);
		this.add(btn_Delete);

	}

	// that method will creat line
	public void paintComponent(Graphics g) {

		super.paintComponent(g);

		// vertical line
		g.drawLine(130, 0, 130, 45);
		g.drawLine(260, 0, 260, 45);
		g.drawLine(390, 0, 390, 45);
		g.drawLine(520, 0, 520, 45);
		g.drawLine(650, 0, 650, 45);
		g.drawLine(780, 0, 780, 45);
		g.drawLine(910, 0, 910, 45);
		g.drawLine(960, 0, 960, 45);

	}

}
