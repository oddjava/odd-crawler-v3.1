package view.settings;

import java.awt.Color;
import java.awt.Graphics;

import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.text.DateFormatter;

/**
 * 
 * @author Ganesh this class is used for creating table pan
 *
 */
public class AddCrawlerPageTableJPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JLabel lbl_CrwalerName = null;
	public JTextField txt_CrwalerName = null;

	public JLabel lbl_ModuleName = null;
	public JLabel lbl_SeedURL = null;
	public JLabel lbl_NLP = null;
	public JLabel lbl_NER = null;
	public JLabel lbl_Image = null;
	public JLabel lbl_Categration = null;
	public JLabel lbl_Translation = null;
	public JLabel lbl_Schedule = null;

	public JCheckBox chkbox_SeddURL = null;
	public JCheckBox chkbox_NLP = null;
	public JCheckBox chkbox_NER = null;
	public JCheckBox chkbox_Image = null;
	public JCheckBox chkbox_Categaration = null;
	public JCheckBox chkbox_Translation = null;
	public JCheckBox chkbox_SchduleCrwal = null;

	public JButton btn_UploadFile_SeedURL = null;
	public JButton btn_UploadFile_NLP = null;
	public JButton btn_UploadFile_Categration = null;

	public JButton btn_Back = null;
	public JButton btn_Apply = null;

	public String temp_crawlerName = null;

	Calendar calendar = null;
	SpinnerDateModel timemodel = null;
	public JSpinner spinner = null;

	public JComboBox JCombo_days = null;
	String[] noofdays = { "Days", "0", "1", "2", "3", "4", "5", "6", "7" };

	/**
	 * this constructor create GUI and add component of JTable
	 * 
	 */
	public AddCrawlerPageTableJPanel() {

		lbl_CrwalerName = new JLabel("Crawler Name");
		txt_CrwalerName = new JTextField();

		JCombo_days = new JComboBox(noofdays);

		calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 24); // 24 == 12 PM == 00:00:00
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);

		timemodel = new SpinnerDateModel();
		timemodel.setValue(calendar.getTime());
		spinner = new JSpinner(timemodel);
		JSpinner.DateEditor editor = new JSpinner.DateEditor(spinner, "hh:mm:aa");// hh:mm:aa
																					// for
																					// am
																					// pm
																					// and
																					// HH:mm:ss
																					// for
																					// 24
																					// hours
		DateFormatter formatter = (DateFormatter) editor.getTextField().getFormatter();
		formatter.setAllowsInvalid(false);
		formatter.setOverwriteMode(true);
		spinner.setEditor(editor);

		lbl_ModuleName = new JLabel("Module Name");
		lbl_SeedURL = new JLabel("SeedURL");
		lbl_NLP = new JLabel("NLP");
		lbl_NER = new JLabel("NER");
		lbl_Image = new JLabel("Image");
		lbl_Categration = new JLabel("Categorization");
		lbl_Translation = new JLabel("Translation");
		lbl_Schedule = new JLabel("Schedule Crawl");

		chkbox_SeddURL = new JCheckBox();
		chkbox_NLP = new JCheckBox();
		chkbox_NER = new JCheckBox();
		chkbox_Image = new JCheckBox();
		chkbox_Categaration = new JCheckBox();
		chkbox_Translation = new JCheckBox();
		chkbox_SchduleCrwal = new JCheckBox();

		btn_UploadFile_SeedURL = new JButton("Upload File");
		btn_UploadFile_NLP = new JButton("Upload File");
		btn_UploadFile_Categration = new JButton("Upload File");

		btn_Back = new JButton("Back");
		btn_Apply = new JButton("Apply");

		lbl_CrwalerName.setBounds(25, 20, 100, 15);
		txt_CrwalerName.setBounds(150, 20, 150, 20);

		lbl_ModuleName.setBounds(25, 60, 100, 15);
		lbl_SeedURL.setBounds(25, 100, 100, 15);
		lbl_NLP.setBounds(25, 140, 100, 15);
		lbl_NER.setBounds(25, 180, 100, 15);
		lbl_Image.setBounds(25, 220, 100, 15);
		lbl_Categration.setBounds(25, 260, 100, 15);
		lbl_Translation.setBounds(25, 300, 100, 15);
		lbl_Schedule.setBounds(25, 340, 100, 15);

		chkbox_SeddURL.setBounds(235, 100, 20, 20);
		chkbox_NLP.setBounds(235, 140, 20, 20);
		chkbox_NER.setBounds(235, 180, 20, 20);
		chkbox_Image.setBounds(235, 220, 20, 20);
		chkbox_Categaration.setBounds(235, 260, 20, 20);
		chkbox_Translation.setBounds(235, 300, 20, 20);
		chkbox_SchduleCrwal.setBounds(235, 340, 20, 20);

		/*
		 * btn_UploadFile_SeedURL.setBounds(355, 55, 120,20 );
		 * btn_UploadFile_NLP.setBounds(355, 95, 120, 20);
		 * btn_UploadFile_Categration.setBounds(355, 218, 120, 20);
		 */

		btn_UploadFile_SeedURL.setBounds(355, 95, 120, 20);
		btn_UploadFile_NLP.setBounds(355, 135, 120, 20);
		btn_UploadFile_Categration.setBounds(355, 258, 120, 20);

		spinner.setBounds(335, 340, 80, 20);
		JCombo_days.setBounds(418, 340, 80, 20);

		btn_UploadFile_SeedURL.setEnabled(false);
		btn_UploadFile_NLP.setEnabled(false);
		btn_UploadFile_Categration.setEnabled(false);

		btn_Back.setBounds(5, 380, 120, 20);
		btn_Apply.setBounds(375, 380, 120, 20);

		this.setLayout(null);
		this.setBounds(400, 80, 500, 410);// x,y,w,h

		this.add(lbl_CrwalerName);
		this.add(txt_CrwalerName);

		this.add(lbl_ModuleName);
		this.add(lbl_SeedURL);
		this.add(lbl_NLP);
		this.add(lbl_NER);
		this.add(lbl_Image);
		this.add(lbl_Categration);
		this.add(lbl_Translation);
		this.add(lbl_Schedule);

		this.add(chkbox_SeddURL);
		this.add(chkbox_NLP);
		this.add(chkbox_NER);
		this.add(chkbox_Image);
		this.add(chkbox_Categaration);
		this.add(chkbox_Translation);
		this.add(chkbox_SchduleCrwal);

		this.add(btn_UploadFile_SeedURL);
		this.add(btn_UploadFile_NLP);
		this.add(btn_UploadFile_Categration);
		this.add(spinner);
		this.add(JCombo_days);

		this.add(btn_Back);
		this.add(btn_Apply);

		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	}

	/**
	 * this method create lines
	 */
	public void paintComponent(Graphics g) {

		super.paintComponent(g);

		// Horizontal line
		g.drawLine(0, 45, 500, 45);
		g.drawLine(0, 85, 500, 85);
		g.drawLine(0, 125, 500, 125);
		g.drawLine(0, 165, 500, 165);
		g.drawLine(0, 207, 500, 207);
		g.drawLine(0, 250, 500, 250);
		g.drawLine(0, 290, 500, 290);
		g.drawLine(0, 330, 500, 330);
		g.drawLine(0, 370, 500, 370);

		// vertical Line
		g.drawLine(166, 45, 166, 370);
		g.drawLine(332, 45, 332, 370);

	}

}
