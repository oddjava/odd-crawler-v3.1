package view.settings;

import static com.mongodb.client.model.Filters.eq;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.text.DateFormatter;

import org.bson.Document;
import org.joda.time.DateTime;

import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

import view.login.LoginPage;

/**
 * @author Ganesh This class is holding Row panel (SettingPageRowJpanel object
 *         means row)
 */

public class SettingPageRowHolderJPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JLabel lbl_Crawler = null;
	JLabel lbl_NLP = null;
	JLabel lbl_NER = null;
	JLabel lbl_Image = null;
	JLabel lbl_Category = null;
	JLabel lbl_Translation = null;
	JLabel lbl_Days = null;
	JLabel lbl_ScheduledTime = null;
	JLabel lbl_EditDelete = null;
	public Map<String, SettingPageRowJPanel> rowMap = null;

	/**
	 * It is default constructor here we create Row Holder JPanel and add its
	 * row(SettingPageRowJpanel object means row)
	 */
	public SettingPageRowHolderJPanel() {

		lbl_Crawler = new JLabel("Crawler");
		lbl_NLP = new JLabel("NLP");
		lbl_NER = new JLabel("NER");
		lbl_Image = new JLabel("Image");
		lbl_Category = new JLabel("Category");
		lbl_Translation = new JLabel("Translation");
		lbl_ScheduledTime = new JLabel("Scheduled Time");
		lbl_Days = new JLabel("Days");
		lbl_EditDelete = new JLabel("Edit/Delete");

		// this is header for column
		lbl_Crawler.setBounds(25, 15, 50, 15);
		lbl_NLP.setBounds(175, 15, 50, 15);
		lbl_NER.setBounds(305, 15, 50, 15);
		lbl_Image.setBounds(430, 15, 50, 15);
		lbl_Category.setBounds(550, 15, 70, 15);
		lbl_Translation.setBounds(675, 15, 70, 15);
		lbl_ScheduledTime.setBounds(790, 15, 100, 15);
		lbl_Days.setBounds(920, 15, 70, 15);
		lbl_EditDelete.setBounds(1030, 15, 100, 15);

		rowMap = gettingRowCollection();// it will call gettingRowCollecton
										// method and it will return Hash map
										// for more information please check
										// method code

		this.setLayout(null);
		this.setBounds(0, 0, 1200, rowMap.size() * 45 + 45);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		this.add(lbl_Crawler);
		this.add(lbl_NLP);
		this.add(lbl_NER);
		this.add(lbl_Image);
		this.add(lbl_Category);
		this.add(lbl_Translation);
		this.add(lbl_Days);
		this.add(lbl_ScheduledTime);
		this.add(lbl_EditDelete);

		// here we adding row(SettingPageRowJpanel object means row) to Setting
		// page Row holder panel
		// We are using map because we fetch row from database so every time
		// size of row will be change from database records
		// In map we have added SettingPageRowJpanel object and key as string
		for (Map.Entry<String, SettingPageRowJPanel> entry : rowMap.entrySet()) {

			this.add(entry.getValue());
			// System.out.println(entry.getKey()+" :
			// "+entry.getValue().lbl_CrawlerName.getText());
		}

	}

	/**
	 * @return HashMap This method Fetch data from database row by row and it
	 *         will store in SettingPageRowJPanel Class and SettingPageRowJPanel
	 *         object will be add to map and this HashMap will be return by
	 *         method
	 */
	private Map<String, SettingPageRowJPanel> gettingRowCollection() {
		long rowCount = 1;
		int position = 0;
		final Map<String, SettingPageRowJPanel> rowCollection = new HashMap<String, SettingPageRowJPanel>();
		// Here Mongo Data base Connection will be done for more information
		// please check ConnectMongoDb class and its methods
		MongoDatabase db = ConnectMongoDB.connectToMongoDB();
		// rowCount=db.getCollection("settings").count();//it will get count
		// document or row

		// Fetching documents from mongodb
		FindIterable<Document> iterable = db.getCollection("settings").find();

		iterable.forEach(new Block<Document>() {
			int position = 0;

			@Override
			public void apply(Document document) {
				// TODO Auto-generated method stub
				SettingPageRowJPanel row = new SettingPageRowJPanel();
				// System.out.println(position);
				// System.out.println(document.get("systemName"));
				// now here exactly will be store data in row
				// object(SettingPageRowJpanel object means row)
				row.lbl_CrawlerName.setText(document.getString("systemName"));
				if (document.getString("nlp").equals("yes"))
					row.chk_NLP.setSelected(true);
				if (document.getString("ner").equals("yes"))
					row.chk_NER.setSelected(true);
				if (document.getString("image").equals("yes"))
					row.chk_Image.setSelected(true);
				if (document.getString("category").equals("yes"))
					row.chk_Category.setSelected(true);
				if (document.getString("translate").equals("yes"))
					row.chk_translation.setSelected(true);
				

				// row.timemodel.setValue(Calendar.getInstance().getTime());
				row.timemodel.setValue(document.getDate("scheduledTIme"));
				row.spinner = new JSpinner(row.timemodel);
				row.editor = new JSpinner.DateEditor(row.spinner, "hh:mm:aa");
				row.formatter = (DateFormatter) row.editor.getTextField().getFormatter();
				row.formatter.setAllowsInvalid(false);
				row.formatter.setOverwriteMode(true);
				row.spinner.setEditor(row.editor);
				row.spinner.setBounds(805, 15, 80, 20);
				row.spinner.setEnabled(false);
				row.add(row.spinner);
				// row.lbl_Time.setText("");

				row.lbl_Days.setText(document.getString("days"));
				rowCollection.put(document.getString("systemName"), row);// here
																			// key
																			// will
																			// be
																			// set
																			// from
																			// database
																			// as
																			// crawler
																			// name
				row.setBounds(0, position + 45, 1200, 45);
				position = position + 45;
			}
		});
		System.out.println(rowCollection.size());

		return rowCollection; // finally will return all row with keeping on map
	}

	/**
	 * this method only used for creating lines in component
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		// Horizontal line
		g.drawLine(0, 45, 1200, 45);

		// vertical Line
		g.drawLine(130, 0, 130, 45);
		g.drawLine(260, 0, 260, 45);
		g.drawLine(390, 0, 390, 45);
		g.drawLine(520, 0, 520, 45);
		g.drawLine(650, 0, 650, 45);
		g.drawLine(780, 0, 780, 45);
		g.drawLine(910, 0, 910, 45);
		g.drawLine(960, 0, 960, 45);

	}

	@Override
	public Dimension getPreferredSize() {
		// System.out.println(rowMap.size()*45+45);

		return new Dimension(1200, rowMap.size() * 45 + 45);
	}

}
