package view.trainingSetPage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

import mongoDBConnection.GetConnection;
import view.frameSetting.JFrameSettings;
import view.topPanel.Navigate;
import view.topPanel.TopPanel;

/**
 * This page is to edit required train files and save, modify the bin file
 * 
 * @author Sourabh
 *
 */
public class TrainingSetPage extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JFrame NLPframe = JFrameSettings.settings("NLP and category trainning set page");
	private JLabel label1 = new JLabel("Trainning Set:");
	String[] set = { "Buyers", "WebinarPodcast", "Events" };
	private JComboBox<String> nlpcombo = new JComboBox<>(set);
	private JTextArea text = new JTextArea();
	private JButton undo = new JButton("Undo");
	private JButton save = new JButton("Save");
	DBCollection coll = GetConnection.connect("nlp");

	/**
	 * This class creates the frame and calls read()
	 * 
	 * @throws IOException
	 *             IOException
	 */
	public TrainingSetPage() throws IOException {
		JMenuBar newMenuBar = TopPanel.getPanel();
		NLPframe.setJMenuBar(newMenuBar);

		label1.setBounds(100, 50, 100, 20);
		NLPframe.add(label1);

		nlpcombo.setBounds(200, 50, 200, 20);
		NLPframe.add(nlpcombo);
		nlpcombo.addActionListener(this);

		JScrollPane scroll = new JScrollPane(text);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		NLPframe.add(scroll);
		scroll.setBounds(200, 100, 1000, 500);

		read();
		save.setBounds(200, 620, 100, 20);
		NLPframe.add(save);
		save.addActionListener(this);

		undo.setBounds(400, 620, 100, 20);
		NLPframe.add(undo);
		undo.addActionListener(this);

		text.setLineWrap(true);
		text.setEditable(true);
		text.setVisible(true);

		Navigate navigate = new Navigate(NLPframe);

		TopPanel.dashboard.addMenuListener(navigate);
		TopPanel.settings.addMenuListener(navigate);
		TopPanel.logout.addMenuListener(navigate);
		TopPanel.training.addMenuListener(navigate);
		TopPanel.verification.addMenuListener(navigate);
		TopPanel.domainStats.addActionListener(navigate);
		TopPanel.systemStats.addActionListener(navigate);
	}

	public static void main(String[] args) throws IOException {
		new TrainingSetPage();

	}

	/**
	 * This methods reads the train file and writes in the text area
	 */
	public void read() {
		text.setText("");// clearing the text area
		Reader reader = null;
		try {
			reader = new FileReader(new File(getname()));// creating reader for
															// selected train
															// file
			text.read(reader, "The force is strong with this one");// reading
																	// from text
																	// area
		} catch (Exception exp) {
			exp.printStackTrace();
		} finally {
			try {
				reader.close();// closing reader
			} catch (Exception exp) {
			}
		}

	}

	/**
	 * It reads the edited text area and writes the same in train file. Builds
	 * bin file from train file
	 * 
	 * @throws IOException
	 *             IOException
	 */
	public void write() throws IOException {
		String name = nlpcombo.getSelectedItem().toString();
		try {
			BufferedWriter fileOut = new BufferedWriter(new FileWriter(getname()));
			fileOut.write(text.getText());// writting buffered text in text area
			fileOut.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		String cmd1 = "cd apache-opennlp-1.6.0-bin &&cd apache-opennlp-1.6.0 &&cd bin &&opennlp DoccatTrainer -model "
				+ name + ".bin -lang en -data " + name + ".train -encoding UTF-8";// command
		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", cmd1);// writting
																			// command
																			// on
																			// command
																			// prompt
		builder.redirectErrorStream(true);
		Process p = builder.start();// executing command
		BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while (true) {
			line = r.readLine();
			if (line == null) {
				break;
			}
			// System.out.println(line);
		}

		JOptionPane.showMessageDialog(NLPframe, "Saved successfully");

	}

	/**
	 * 
	 * @return Name of train or bin file
	 */
	public String getname() {

		DBCursor cursor = coll.find(new BasicDBObject("id", nlpcombo.getSelectedItem().toString()));
		String name = (String) cursor.one().get("name");// getting name from
														// database collection
		return name;
	}

	@Override
	/**
	 * This is the action listener for save and undo button
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == save) {
			int reply = JOptionPane.showConfirmDialog(null, "Are you sure you want to save changes?", "Save Changes",
					JOptionPane.YES_NO_OPTION);
			if (reply == JOptionPane.YES_OPTION) {
				try {
					write();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

		}
		if (e.getSource() == undo) {

			int reply = JOptionPane.showConfirmDialog(null, "Are you sure you want to undo changes?", "Undo Changes",
					JOptionPane.YES_NO_OPTION);
			if (reply == JOptionPane.YES_OPTION) {
				read();
			}

		}
		if (e.getSource() == nlpcombo) {
			read();

		}
	}
}
