package dateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Date{
	/*If given paratmeter is 0 then we get current date. If given 1 then we get yesterdays date.
	 *Ths method is used to get any date from current date.
	 */
	public static String getDate(int x)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -x);
		return dateFormat.format(cal.getTime());
	}
	
	public static String format_date(String date) {
		String d;
		d = date.toString();
		d = (new String(d)).substring(4, 11);
		d = remove_space(d);
		if (d.contains("Jan"))
			d.replace("Jan", "01");
		else if (d.contains("Feb"))
			d = d.replace("Feb", "02");
		else if (d.contains("Mar"))
			d = d.replace("Mar", "03");
		else if (d.contains("Apr"))
			d = d.replace("Apr", "04");
		else if (d.contains("May"))
			d = d.replace("May", "05");
		else if (d.contains("Jun"))
			d = d.replace("Jun", "06");
		else if (d.contains("Jul"))
			d = d.replace("Jul", "07");
		else if (d.contains("Aug"))
			d = d.replace("Aug", "08");
		else if (d.contains("Sep"))
			d = d.replace("Sep", "09");
		else if (d.contains("Oct"))
			d = d.replace("Oct", "10");
		else if (d.contains("Nov"))
			d = d.replace("Nov", "11");
		else if (d.contains("Dec"))
			d = d.replace("Dec", "12");
		d = "2016-".concat(d);
		return d = d.substring(0, 10);
	}
	
	public static String remove_space(CharSequence keyword) {
		String pat = "\\s+";
		String plus = "-";
		Pattern pattern = Pattern.compile(pat);
		Matcher mat = pattern.matcher(keyword);
		return mat.replaceAll(plus);
	}
}
