package apacheNLP;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import opennlp.tools.doccat.DoccatModel;
import opennlp.tools.doccat.DocumentCategorizerME;

public class NLP {
	public static String DocumentCategorizer(String text,String fileName) throws IOException {

		//File test = new File("D:\\Ajit\\apache-opennlp-1.6.0-bin\\apache-opennlp-1.6.0\\bin\\en-doccat.bin");
		File test = new File(".\\"+fileName);
		String classificationModelFilePath = test.getAbsolutePath();
		DocumentCategorizerME classificationME = new DocumentCategorizerME(
				new DoccatModel(new FileInputStream(classificationModelFilePath)));
		String documentContent = text;
		double[] classDistribution = classificationME.categorize(documentContent);

		String predictedCategory = classificationME.getBestCategory(classDistribution);
		System.out.println("Model prediction : " + predictedCategory);
		return predictedCategory;
	}
}
