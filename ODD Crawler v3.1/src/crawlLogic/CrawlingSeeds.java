package crawlLogic;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import mongoDBConnection.GetConnection;
import view.login.LoginPage;

public class CrawlingSeeds {

	public String url, domainName, moduleName, nextCrawlDate, type, description, keyword, regex, noRegex, subMain = null;
	public int bucketNumber;

	public CrawlingSeeds(String url, String domainName, String moduleName, String nextCrawlDate, int bucketNumber,
			String type, String description, String keyword, String regex, String noRegex, String subMain) {
		super();
		this.url = url;
		this.domainName = domainName;
		this.moduleName = moduleName;
		this.nextCrawlDate = nextCrawlDate;
		this.bucketNumber = bucketNumber;
		this.type = type;
		this.description = description;
		this.keyword = keyword;
		this.regex = regex;
		this.subMain = subMain;
		this.noRegex = noRegex;
	}

	public static ArrayList<CrawlingSeeds> dbSeedUrl() {
		String url, domainName, moduleName, nextCrawlDate, type, description = "", keyword = "", regex = "", noRegex = "",
				subMain = "", bucket = "";
		int bucketNumber;
		String systemName = LoginPage.gettypename();
		//String systemName = "NER C";
		ArrayList<CrawlingSeeds> seedUrl = new ArrayList<>();
		DBCollection domainDB = GetConnection.connect("domain");
		BasicDBObject domainQuery = new BasicDBObject();

		DBCollection seedUrlDB = GetConnection.connect("SeedUrl");
		BasicDBObject seedUrlQuery = new BasicDBObject();
		seedUrlQuery.put("systemName", systemName); // parameterized query. here
													// we provide the condition

		BasicDBObject domainQuery1 = new BasicDBObject();
		domainQuery1.put("systemName", systemName); // parameterized query. here
		domainQuery1.put("regex", new BasicDBObject("$ne", "no regex"));
		DBCursor domain1Cursor = domainDB.find(domainQuery1);
		while (domain1Cursor.hasNext()) {
			// all set of domains
			//DBCursor seedUrlCursor = seedUrlDB.find(seedUrlQuery).sort(new BasicDBObject("nextCrawlDate", 1).append("domainUrl", domain1Cursor.next().get("domainName"))).limit(50); // 1
			BasicDBObject seedUrlQuery1 = new BasicDBObject();
			seedUrlQuery1.put("domainUrl", domain1Cursor.next().get("domainName"));
			DBCursor seedUrlCursor = seedUrlDB.find(seedUrlQuery1).sort(new BasicDBObject("nextCrawlDate", 1)).limit(50); // 1;
			// means
			// sort
			// in
			// asc
			// order
			// and
			// -1
			// means
			// in
			// desc
			// order
			int i = 1;
			CrawlingSeeds seeds;
			System.out.println("Size is "+seedUrlCursor.size());
			while (seedUrlCursor.hasNext()) {
				// System.out.println(cursor.next());
				DBObject object = seedUrlCursor.next();
				url = object.get("url").toString();
				domainName = object.get("domainUrl").toString();
				subMain = object.get("subdomain/maindomain").toString();
				bucket = object.get("bucketNumber").toString();
				bucketNumber = Integer.parseInt(bucket);
				nextCrawlDate = object.get("nextCrawlDate").toString();
				moduleName = (String) object.get("moduleName");
				type = (String) object.get("type");
				//System.out.println("Type in Seed is "+type);
				domainQuery.put("domainName", domainName);
				DBCursor domainCursor = domainDB.find(domainQuery);
				while (domainCursor.hasNext()) {
					DBObject ob = domainCursor.next();
					description = (String) ob.get("description");
					keyword = (String) ob.get("keyword");
					regex = (String) ob.get("regex");
					noRegex = (String) ob.get("noRegex");
				}
				domainQuery.clear();
				//System.out.println("Regex is " + regex + " for " + url);
				if (!regex.equals("no regex")) {
					seeds = new CrawlingSeeds(url, domainName, moduleName, nextCrawlDate, bucketNumber, type,
							description, keyword, regex, noRegex, subMain);
					seedUrl.add(seeds);
					//System.out.println("Seed is " + seeds.url);
				}

				i++;
			}

		}
		System.out.println("Seed Url size is "+seedUrl.size());
		return seedUrl;
	}

	/*
	 * public static ArrayList<CrawlingSeeds>
	 * crawlSeedUrl(ArrayList<CrawlingSeeds> seedUrl) {
	 * 
	 * CrawlingSeeds seeds; // Reference of Crawling seeds class String url =
	 * "", domainName, moduleName = null, nextCrawlDate = null, type = null,
	 * description = null, keyword = null, regex = null, subMain = null; int
	 * bucketNumber = 0; // Local variables of all the columns present in //
	 * seedUrl and domain tables
	 * 
	 * DBCollection seedUrlDB = GetConnection.connect("SeedUrl"); // connected
	 * // to // SeedUrl // table BasicDBObject seedUrlQuery = new
	 * BasicDBObject();
	 * 
	 * DBCollection domainDB = GetConnection.connect("domain"); // Connected to
	 * // domain // table BasicDBObject domainQuery = new BasicDBObject();
	 * 
	 * Random randomGenerator = new Random(); // Object of Random class to //
	 * create random number ArrayList<CrawlingSeeds> crawl = new
	 * ArrayList<CrawlingSeeds>(); // Final // arraylist // to // return //
	 * which // contains // object // of // current // class
	 * 
	 * // In this logic we check wether previous domain and next domain are //
	 * same. If same then we fetch different url of different domain. int index,
	 * cnt = 0; String previousUrl = "http://www.sample.com"; String nextUrl =
	 * ""; previousUrl = seedUrl.get(0).url; nextUrl = seedUrl.get(1).url;
	 * String previousDomain = "", nextDomain = ""; while (cnt < seedUrl.size())
	 * { index = randomGenerator.nextInt(seedUrl.size()); nextUrl =
	 * seedUrl.get(index).url; try { DBCursor seedUrlCursor =
	 * seedUrlDB.find(seedUrlQuery); seedUrlQuery.put("url", previousUrl); //
	 * parameterized query. // here we // fetch domain name for // provided url
	 * seedUrlCursor = seedUrlDB.find(seedUrlQuery); while
	 * (seedUrlCursor.hasNext()) { // System.out.println(cursor.next());
	 * previousDomain = (String) seedUrlCursor.next().get("domainUrl"); }
	 * 
	 * } catch (Exception e) { previousDomain = ""; } try {
	 * seedUrlQuery.clear(); seedUrlQuery.put("url", nextUrl); // parameterized
	 * query. here // we fetch // domain name for provided url DBCursor
	 * seedUrlCursor = seedUrlDB.find(seedUrlQuery); while
	 * (seedUrlCursor.hasNext()) { // System.out.println(cursor.next());
	 * DBObject object = seedUrlCursor.next(); nextDomain = (String)
	 * object.get("domainUrl"); // url = (String) object.get("url"); subMain =
	 * (String) object.get("subdomain/maindomain"); bucketNumber = (int)
	 * object.get("bucketNumber"); nextCrawlDate = (String)
	 * object.get("nextCrawlDate"); type = (String) object.get("type");
	 * moduleName = (String) object.get("moduleName"); domainQuery.clear();
	 * domainQuery.put("domainName", nextDomain); DBCursor domainCursor =
	 * domainDB.find(domainQuery); while (domainCursor.hasNext()) { DBObject ob
	 * = domainCursor.next(); description = (String) ob.get("description");
	 * keyword = (String) ob.get("keyword"); regex = (String) ob.get("regex"); }
	 * } } catch (Exception e) { // TODO Auto-generated catch block nextDomain =
	 * ""; } // System.out.println("Next"); // System.out.println(nextUrl+" "
	 * +nextDomain); if (!previousDomain.contentEquals(nextDomain)) { cnt++;
	 * seeds = new CrawlingSeeds(nextUrl, nextDomain, moduleName, nextCrawlDate,
	 * bucketNumber, type, description, keyword, regex, subMain);
	 * crawl.add(seeds); seedUrl.remove(index); previousUrl = nextUrl; } }
	 * 
	 * boolean flag = true; for (int i = 0; i < seedUrl.size(); i++) { //
	 * System.out.println(links.get(i)); for (int j = 0; j < seedUrl.size();
	 * j++) { if (seedUrl.get(i).url.equals(crawl.get(j).url)) { flag = true;
	 * break; } else { flag = false; } } if(flag = false) {
	 * crawl.add(seedUrl.get(i)); } }
	 * 
	 * return crawl; }
	 */

	/*
	 * public static String getDomainName(String url) throws URISyntaxException
	 * { URI uri = new URI(url); String domain = uri.getHost(); return
	 * domain.startsWith("www.") ? domain.substring(4) : domain; }
	 */

	public static ArrayList<CrawlingSeeds> crawlSeedUrl(ArrayList<CrawlingSeeds> seedUrl) {
		DBCollection domainDB = GetConnection.connect("SeedUrl");
		BasicDBObject seedUrlquery = new BasicDBObject();
		ArrayList<CrawlingSeeds> crawldata = new ArrayList<>();

		/*
		 * for (int i = 0; i < seedUrl.size(); i++) {
		 * System.out.println(seedUrl.get(i).url); }
		 */

		int i = 0;

		while (i != seedUrl.size()) {
			// System.out.println("i= "+i+" db size "+dbdata.size());
			if (!crawldata.contains(seedUrl.get(i))) {
				seedUrlquery.put("url", seedUrl.get(i).url);
				DBCursor cursor = domainDB.find(seedUrlquery);
				String nxtDomain = "";
				while (cursor.hasNext()) {
					nxtDomain = cursor.next().get("domainUrl").toString();
					// System.out.println("NEXT DOMAIN IS "+nxtDomain);
				}
				seedUrlquery.clear();
				String prevDomain = "";
				if (!crawldata.isEmpty()) {
					// System.out.println("Crawldata is not empty");
					// prevDomain = getDomainName(crawldata.get(crawldata.size()
					// - 1));
					seedUrlquery.put("url", crawldata.get(crawldata.size() - 1).url);
					DBCursor cursor1 = domainDB.find(seedUrlquery);
					while (cursor1.hasNext()) {
						// System.out.println(cursor.next());
						prevDomain = cursor1.next().get("domainUrl").toString();
						// System.out.println("NEXT DOMAIN IS "+prevDomain);
					}
					seedUrlquery.clear();

				} else {
					prevDomain = "";
					// System.out.println("Crawldata is empty");
				}
				if (!prevDomain.equals(nxtDomain)) {
					crawldata.add(seedUrl.get(i));
					// System.out.println("Added "+seedUrl.get(i));
					i = 0;
				} else {
					i++;
					// System.out.println("i++");
				}
			} else {
				i++;
				// System.out.println("i++");
			}
		}

		for (int j = 0; j < seedUrl.size(); j++) {
			if (!crawldata.contains(seedUrl.get(j))) {
				crawldata.add(seedUrl.get(j));
				// System.out.println("Added "+seedUrl.get(i));
			}
		}

		/*
		 * System.out.println("Crawl data is "); for (int j = 0; j <
		 * crawldata.size(); j++) { System.out.println(crawldata.get(j).url); }
		 */

		return crawldata;
	}

	public static void main(String[] args) {

		ArrayList<CrawlingSeeds> list = CrawlingSeeds.dbSeedUrl();
		//System.out.println("DB Url");
		for (int i = 0; i < list.size(); i++) {
			//System.out.println(list.get(i).url);
		}

		ArrayList<CrawlingSeeds> list1 = CrawlingSeeds.crawlSeedUrl(list);
		//System.out.println("Crawling seeds " + list1.size());

		for (int i = 0; i < list1.size(); i++) {
			System.out.println(list1.get(i).url);
			System.out.println(list1.get(i).type);
			System.out.println("No Regex "+list1.get(i).noRegex);
		}

	}
}

class seedData {
	public String url, domainName, moduleName, nextCrawlDate, type, description, keyword, regex, submain, noRegex;
	public int bucketNumber;

	public seedData(String url, String domainName, String moduleName, String nextCrawlDate, int bucketNumber,
			String type, String description, String keyword, String regex, String noRegex, String submain) {
		super();
		this.url = url;
		this.domainName = domainName;
		this.moduleName = moduleName;
		this.nextCrawlDate = nextCrawlDate;
		this.bucketNumber = bucketNumber;
		this.type = type;
		this.description = description;
		this.keyword = keyword;
		this.regex = regex;
		this.submain = submain;
		this.noRegex = noRegex;
	}
}
