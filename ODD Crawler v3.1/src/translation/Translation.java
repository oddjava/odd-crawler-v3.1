package translation;

import com.memetix.mst.detect.Detect;
import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

public class Translation {
	public static boolean languageDetectIsEnglish(String str) {
		Language lang = null;
		try {
			lang = Detect.execute((String) str);
			if (lang.toString().equals("en")) {
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}

	public static String languageTranslate(String langstr) {
		String translatedText = null;
		try {
			translatedText = Translate.execute((String) langstr, (Language) Language.ENGLISH);
		} catch (Exception e) {
		}
		return translatedText;
	}
}
