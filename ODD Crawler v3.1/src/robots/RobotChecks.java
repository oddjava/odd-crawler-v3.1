package robots;

import java.io.BufferedReader;

import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import addDomainLogic.DomainName;

public class RobotChecks {

	public static String metaRobots(Document doc) throws Exception {
		// Add all the links o=in db related to this domain
		try {
			//Document doc = Jsoup.connect(url).timeout(0x927c0).get();
			String robots = doc.select("META[NAME=robots]").get(0).attr("CONTENT");
			if (robots.contains("noindex")) {
				// System.out.println("cannot index "+url);
				return "noindex";
			} else {
				// System.out.println("can index "+url);
				return "index";
			}
		} catch (Exception e) {
			return "index";
		}
	}

	public static ArrayList<String> robotsTXT(String url) {
		ArrayList<String> disAllowList = new ArrayList<String>();
		String domain = DomainName.getMainDomainName(url);
		try {
			URL robotURL = new URL(domain+"/robots.txt");
			BufferedReader in = new BufferedReader(new InputStreamReader(robotURL.openStream()));
			String line = null;
			boolean flag = false;
			while ((line = in.readLine()) != null) {
				if (line.contains("User-agent")) {

					if ((line.substring(line.indexOf(":") + 1, line.length()).trim()).contains("*")
							&& !(line.substring(line.indexOf(":") + 1, line.length()).trim())
									.contains("Mediapartners-Google*")) {
						flag = true;
					} else
						flag = false;
				}
				if (flag == true) {
					if (line.contains("Disallow"))
						disAllowList.add(url.concat((line.substring(line.indexOf(":") + 1, line.length()).trim())));
				}
			}
			in.close();
		} catch (Exception e) {
			disAllowList.add("null");
		}
		return disAllowList;
	}
}
