package mongoDBConnection;

import org.bson.Document;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class GetConnection {

	public static DBCollection connect(String collectionName)
	{
		DB db = null;
		DBCollection coll=null;
		try{
			
	         // To connect to mongodb server
	         MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
			 //db=mongoClient.getDB("crawler");
	         db=mongoClient.getDB("crawler");
	         System.out.println("Connect to database successfully");
	         coll = db.getCollection(collectionName);
	         System.out.println("Collection "+collectionName+" selected successfully");
	      }catch(Exception e){
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      }
		return coll;
	}
	public static void main(String[] args) {
		GetConnection.connect("domain");
	}
}
